package Week02;



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class HourlyTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class HourlyTest {
    /**
     * Default constructor for test class HourlyTest.
     */
    public HourlyTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.setUp();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.tearDown();
    }
    
    
    @Test
    public void testConstruction() {
        Hourly e = new Hourly("Albus Dumbledore",
            new Money(50.0), 10);
        assertNotNull(e);
    }
    
    @Test
    public void testHourlyCalculatePay1() {
        Hourly e = new Hourly("Albus Dumbledore",
            new Money(10.0), 30);
        assertEquals(new Money(300.0), e.calculatePay());
    }
    
    @Test
    public void testHourlyCalculatePay2() {
        Employee e = new Hourly("Albus Dumbledore",
            new Money(10.0), 40);
        assertEquals(new Money(400.0), e.calculatePay());
    }

    @Test
    public void testHourlyCalculatePay3() {
        Hourly e = new Hourly("Albus Dumbledore",
             new Money(10.0), 50);
        assertEquals(new Money(550.0), e.calculatePay());
    }
    
    @Test
    public void testToString1() {
        Hourly e = new Hourly("Albus Dumbledore",
            new Money(10.0), 50);
        String s = e.toString();
        assertTrue(s.contains("name:Albus Dumbledore"));
        assertTrue(s.contains("payRate:$10.00"));
        assertTrue(s.contains("hoursWorked:50"));
    }
    
    @Test
    public void testEdit() {
        String input = "Fred Weasley\n100\n30\n";
        Hourly e = new Hourly("Albus Dumbledore",
            new Money(50.0), 20);
        Console console = new Console(input);
        e.edit(console);
        assertEquals("Fred Weasley", e.getName());
        assertEquals(new Money(100.0), e.getPayRate());
        assertEquals(30.0, e.getHoursWorked(), 0.0001);
    }

}
