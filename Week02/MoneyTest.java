package Week02;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class MoneyTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class MoneyTest {

    private static final double DELTA = 1E-10;

    /**
     * Default constructor for test class MoneyTest.
     */
    public MoneyTest() {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
    }

    /**
     * A sample test case.
     *
     * Process:
     *  1. Create an object to test
     *  2. Assert something about the initial object state
     *  3. Call a method
     *  4. Assert something about the return value
     *     or the state of the object after the call
     */
    @Test
    public void testConstructor1() {
        Money money = new Money();
        assertNotNull(money);
        assertEquals(0.0, money.toDouble(), DELTA);
    }

    @Test
    public void testConstructor2() {
        Money money = new Money(5.0);
        assertNotNull(money);
        assertEquals(5.0, money.toDouble(), DELTA);
    }
    
    @Test
    public void testConstructor3() {
        Money money = new Money(3.14);
        assertNotNull(money);
        assertEquals(3.14, money.toDouble(), DELTA);
    }
    
    @Test
    public void testToString1() {
        Money money1 = new Money(3.14);
        assertEquals("$3.14", money1.toString());
        Money money2 = new Money(3.10);
        assertEquals("$3.10", money2.toString());
        Money money3 = new Money(0.01);
        assertEquals("$0.01", money3.toString());
        Money money4 = new Money(-500.25);
        assertEquals("$-500.25", money4.toString());
    }
    
    @Test
    public void testAdd1() {
        Money money1 = new Money(3.14);
        Money money2 = new Money(5.93);
        Money sum = new Money(9.07);
        assertEquals(sum.toString(), money1.add(money2).toString());
        assertEquals(sum, money1.add(money2));
    }
    
    @Test
    public void testSub1() {
        Money money1 = new Money(3.14);
        Money money2 = new Money(5.93);
        Money diff = new Money(-2.79);
        assertEquals(diff.toString(), money1.sub(money2).toString());
        assertEquals(diff, money1.sub(money2));
    }
    
    @Test
    public void testMul1() {
        Money money1 = new Money(3.14);
        Money answer = new Money(6.91);
        assertEquals(answer, money1.mul(2.2));
    }
    
    @Test
    public void testDiv1() {
        Money money1 = new Money(3.14);
        Money answer = new Money(1.43);
        assertEquals(answer, money1.div(2.2));
    }
}