package Week02;

/**
 * Write a description of interface Employee here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public interface Employee extends Person, Comparable<Employee> {
    Money calculatePay();
    void setHireId(int id);
    int getHireId();
    Employee getRaise(double percent);
    Employee edit(Console console);
}
