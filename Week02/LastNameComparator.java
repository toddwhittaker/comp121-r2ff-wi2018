package Week02;
import java.util.Comparator;

/**
 * Write a description of class LastNameComparator here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class LastNameComparator implements Comparator<Employee> {
    public int compare(Employee e1, Employee e2) {
        String myLastName = e1.getName().split(" ")[1];
        String otherLastName = e2.getName().split(" ")[1];
        return myLastName.compareTo(otherLastName);
    }
}
