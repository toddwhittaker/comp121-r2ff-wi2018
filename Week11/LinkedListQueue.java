package Week11;
import java.util.List;
import java.util.LinkedList;

/**
 * Write a description of class LinkedListQueue here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class LinkedListQueue<E> extends AbstractQueue<E> {
    private List<E> list;
    
    public LinkedListQueue() {
        list = new LinkedList<E>();
    }

    /**
     * Inserts the specified element into this queue if it is
     * possible to do so immediately without violating capacity
     * restrictions, returning true upon success and throwing
     * an IllegalStateException if no space is currently available.
     * 
     * @param element - the element to add
     * @returns true if the add was successful
     * @throws IllegalStateException - if the element cannot be
     *     added at this time due to capacity restrictions
     * @throws ClassCastException - if the class of the specified
     *     element prevents it from being added to this queue
     * @throws NullPointerException - if the specified element is
     *     null and this queue does not permit null elements
     * @throws IllegalArgumentException - if some property of this
     *     element prevents it from being added to this queue
     */
    public boolean add(E element) {
        return list.add(element);
    }

    /**
     * Inserts the specified element into this queue if it is
     * possible to do so immediately without violating capacity
     * restrictions. When using a capacity-restricted queue, this
     * method is generally preferable to add(E), which can fail
     * to insert an element only by throwing an exception.
     * 
     * @param element - the element to add
     * @returns true if the add was successful
     * @throws ClassCastException - if the class of the specified
     *     element prevents it from being added to this queue
     * @throws NullPointerException - if the specified element is
     *     null and this queue does not permit null elements
     * @throws IllegalArgumentException - if some property of this
     *     element prevents it from being added to this queue
     */
    public boolean offer(E element) {
        return list.add(element);
    }

    /**
     * Retrieves, but does not remove, the head of this queue,
     * or returns null if this queue is empty.
     * 
     * @returns the head of this queue, or null if the queue is empty
     */
    public E peek() {
        if (isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    /**
     * Retrieves and removes the head of this queue, or returns
     * null if this queue is empty.
     * 
     * @returns the head of this queue, or null if the queue is empty
     */
    public E poll() {
        if (isEmpty()) {
            return null;
        }
        E result = list.get(0);
        list.remove(0);
        return result;
    }

    /**
     * Returns true if the queue contains no elements.
     * 
     * @returns true if this queue contains no elements
     */
    public boolean isEmpty() {
        return list.isEmpty();
    }

    /**
     * Returns the number of elements in this queue.
     * 
     * @returns the numbe of elments in this queue.
     */
    public int size() {
        return list.size();
    }

}
