package Week11;


import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class LinkedListQueueTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class LinkedListQueueTest {
    private Queue<Integer> queue;

    /**
     * Default constructor for test class LinkedListQueueTest.
     */
    public LinkedListQueueTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.setUp();
        queue = new LinkedListQueue<Integer>();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.tearDown();
    }

    @Test
    public void testConstruction() {
        assertNotNull(queue);
        assertEquals(0, queue.size());
        assertTrue(queue.isEmpty());
    }

    @Test
    public void testOffer() {
        assertTrue(queue.offer(3));
        assertEquals(1, queue.size());
        assertFalse(queue.isEmpty());

        assertTrue(queue.offer(5));
        assertTrue(queue.offer(7));
        assertEquals(3, queue.size());
    }

    @Test
    public void testPeek() {
        assertNull(queue.peek());
        queue.offer(3);
        assertEquals(3, queue.peek().intValue());
        queue.offer(5);
        assertEquals(3, queue.peek().intValue());
        for (int i = 0; i < 10; ++i) {
            queue.offer(i);
            assertEquals(3, queue.peek().intValue());
        }
        assertEquals(12, queue.size());
    }

    @Test(expected = java.util.NoSuchElementException.class)
    public void testElement() {
        queue.element();
    }

    @Test
    public void testPoll() {
        queue.offer(3);
        assertEquals(3, queue.poll().intValue());
        assertTrue(queue.isEmpty());

        queue.offer(5);
        queue.offer(7);
        assertEquals(5, queue.poll().intValue());
        assertEquals(7, queue.poll().intValue());
        assertTrue(queue.isEmpty());

        for (int i = 0; i < 10; ++i) {
            queue.offer(i);
        }
        for (int i = 0; i < 10; ++i) {
            assertEquals(i, queue.poll().intValue());
        }
        assertNull(queue.poll());
    }

    @Test
    public void testRemove1() {
        queue.offer(3);
        assertEquals(3, queue.remove().intValue());
        assertTrue(queue.isEmpty());

        queue.offer(5);
        queue.offer(7);
        assertEquals(5, queue.remove().intValue());
        assertEquals(7, queue.remove().intValue());
        assertTrue(queue.isEmpty());

        for (int i = 0; i < 10; ++i) {
            queue.offer(i);
        }
        for (int i = 0; i < 10; ++i) {
            assertEquals(i, queue.remove().intValue());
        }
    }
    
    @Test(expected = java.util.NoSuchElementException.class)
    public void testRemove2() {
        queue.remove();
    }
}
