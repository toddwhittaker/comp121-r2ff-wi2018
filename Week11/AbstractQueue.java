package Week11;

/**
 * Write a description of class AbstractQueue here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public abstract class AbstractQueue<E> implements Queue<E> {
    /**
     * Retrieves, but does not remove, the head of this queue.
     * This method differs from peek only in that it throws an
     * exception if this queue is empty.
     * 
     * @returns the head of this queue
     * @throws NoSuchElementException - if the queue is empty
     */
    public E element() {
        if (isEmpty()) {
            throw new java.util.NoSuchElementException();
        }
        return peek();
    }

    /**
     * Retrieves and removes the head of this queue. This method
     * differs from poll only in that it throws an exception if
     * this queue is empty.
     * 
     * @returns the head of this queue
     * @throws NoSuchElementException - if the queue is empty
     */
    public E remove() {
        if (isEmpty()) {
            throw new java.util.NoSuchElementException();
        }
        return poll();
    }

}
