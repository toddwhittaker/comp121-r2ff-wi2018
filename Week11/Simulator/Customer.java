package Week11.Simulator;

public class Customer {
   private int arrivalTime;
   private int processTime;
   private int finishTime;
   private String name;

   public Customer(String name, int arrivalTime, int processTime) {
      this.name = name;
      this.arrivalTime = arrivalTime;
      this.processTime = processTime;
      this.finishTime = -1;
   }
   
   public void setFinishTime(int finishTime) {
      this.finishTime = finishTime;
   }
   
   public int getProcessTime() {
      return processTime;
   }
   
   public int getWaitTime() {
      if (finishTime < 0) {
         throw new IllegalStateException("Customer not finished");
      }
      return finishTime - (arrivalTime + processTime);
   }
   
   public int getFinishTime() {
      return finishTime;
   }
   
   public int getArrivalTime() {
      return arrivalTime;
   }

   public boolean isFinished() {
      return finishTime < 0;
   }
   
   public String toString() {
      return String.format("Customer %s {arrivalTime:%d, processTime:%d, finishTime:%d}", name, arrivalTime, processTime, finishTime);
   }
}
