package Week11.Simulator;

import java.util.Random;

public class PoissonGenerator implements EventGenerator {
   private Random random;
   private double low;
   private double average;
   private double lastResult;
   private boolean cumulative;

   public PoissonGenerator(double low, double average, boolean cumulative) {
      if (low > average) {
         throw new IllegalArgumentException("low bound must be <= average");
      }
      this.low = low;
      this.average = average;
      this.lastResult = 0;
      this.cumulative = cumulative;
      this.random = new Random();
   }

   @Override
   public double nextEventTime() {
      double delta = -Math.log(1.0 - random.nextDouble()) / (1 / (average - low)) + low;
      if (cumulative) {
         lastResult = lastResult + delta;
         return lastResult;
      }
      return delta;
   }

   @Override
   public void setSeed(long seed) {
      random.setSeed(seed);
   }
}
