package Week11.Simulator;


/**
 * Write a description of class FewestItemsStrategy here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class FewestItemsStrategy extends AbstractQueueStrategy {
    
    public FewestItemsStrategy(int numCashiers) {
        super(numCashiers);
    }

    /**
     * What should happen with a new customer arrives?
     */
    public void customerArrives(Customer customer) {
        int min = 0;
        for (int i = 1; i < cashiers.size(); ++i) {
            if (cashiers.get(min).getItemCount() > cashiers.get(i).getItemCount()) {
                min = i;
            }
        }
        cashiers.get(min).customerArrives(customer);
    }
}
