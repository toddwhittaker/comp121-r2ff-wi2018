package Week11.Simulator;


public interface EventGenerator {
   double nextEventTime();
   void setSeed(long seed);
}
