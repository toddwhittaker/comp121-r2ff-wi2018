package Week11.Simulator;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractQueueStrategy implements QueueStrategy {
    protected List<Cashier> cashiers;

    public AbstractQueueStrategy(int numCashiers) {
        cashiers = new ArrayList<Cashier>(numCashiers);
        for (int i = 0; i < numCashiers; ++i) {
            cashiers.add(new Cashier());
        }
    }

    /**
     * How does the simulation advance?
     */
    public void step() {
        for (Cashier cashier : cashiers) {
            cashier.work();
        }
    }

    /**
     * Figure out how much time cashiers are idled.
     */
    public int getIdleTime() {
        int sum = 0;
        for (Cashier cashier : cashiers) {
            sum += cashier.getIdleTime();
        }
        return sum;
    }
}
