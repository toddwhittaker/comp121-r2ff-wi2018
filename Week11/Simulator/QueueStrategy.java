package Week11.Simulator;

public interface QueueStrategy {

   /**
    * What should happen with a new customer arrives?
    */
   void customerArrives(Customer customer);

   /**
    * How does the simulation advance?
    */
   void step();

   /**
    * Figure out how much time cashiers are idled.
    */
   int getIdleTime();

}
