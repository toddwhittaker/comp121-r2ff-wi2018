package Week11.Simulator;

import java.util.LinkedList;
import java.util.Queue;

public class Cashier {
    private Queue<Customer> queue; // waiting at the register
    private Customer serving;      // customer being served;
    private int idleTime;          // time checking facebook
    
    public Cashier() {
        queue = new LinkedList<Customer>();
        serving = null;
        idleTime = 0;
    }

    public boolean isBusy() {
        return serving != null;
    }

    public void customerArrives(Customer customer) {
        if (customer != null) {
            queue.offer(customer);
        }
    }

    public void work() {
        int time = Clock.instance().getTime();
        if (serving != null && time >= serving.getFinishTime()) {
            serving = null;
        } else {
            // I'm working with this customer.
        }
        if (serving == null) {
            Customer customer = queue.poll();
            if (customer != null) {
                serving = customer;
                customer.setFinishTime(time + customer.getProcessTime());
            } else {
                ++idleTime; // check facebook
            }
        }
    }

    public int getIdleTime() {
        return idleTime;
    }

    public int getLineLength() {
        return queue.size();
    }
    
    public int getItemCount() {
        int sum = 0;
        for (Customer customer : queue) {
            sum += customer.getProcessTime();
        }
        return sum;
    }
}
