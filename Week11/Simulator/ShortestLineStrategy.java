package Week11.Simulator;
import java.util.*;

/**
 * Write a description of class ShortestLineStrategy here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ShortestLineStrategy extends AbstractQueueStrategy {

    public ShortestLineStrategy(int numCashiers) {
        super(numCashiers);
    }

    /**
     * What should happen with a new customer arrives?
     */
    public void customerArrives(Customer customer) {
        int min = 0;
        for (int i = 1; i < cashiers.size(); ++i) {
            if (cashiers.get(min).getLineLength() > cashiers.get(i).getLineLength()) {
                min = i;
            }
        }
        cashiers.get(min).customerArrives(customer);
    }
}
