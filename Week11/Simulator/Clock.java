package Week11.Simulator;

public class Clock {
   private static final Clock instance = new Clock();
   private int time;
   
   public static Clock instance() {
      return instance;
   }
   
   private Clock() {
      reset();
   }
   
   public int getTime() {
      return time; 
   }
   
   public int tick() {
      ++time;
      return getTime();
   }
   
   public void reset() {
      time = 0;
   }
}
