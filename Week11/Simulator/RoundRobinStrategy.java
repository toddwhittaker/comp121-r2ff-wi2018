package Week11.Simulator;
import java.util.*;

/**
 * Write a description of class RoundRobinStrategy here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class RoundRobinStrategy extends AbstractQueueStrategy {
    private int nextCashier;
    
    public RoundRobinStrategy(int numCashiers) {
        super(numCashiers);
        nextCashier = 0;
    }
    
    /**
     * What should happen with a new customer arrives?
     */
    public void customerArrives(Customer customer) {
        cashiers.get(nextCashier).customerArrives(customer);
        nextCashier = (nextCashier + 1) % cashiers.size();
    }
}
