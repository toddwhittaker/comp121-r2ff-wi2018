package Week11.Simulator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

public class Simulator {

    private static final int MAX_CUSTOMERS = 1000;
    private Queue<Customer> waitingCustomers;
    private List<Customer> finishedCustomers;
    private QueueStrategy strategy;
    private Clock clock;

    public Simulator(QueueStrategy strategy, EventGenerator arrivals, EventGenerator processing) {
        this.strategy = strategy;
        waitingCustomers = new LinkedList<Customer>();
        for (int i = 0; i < MAX_CUSTOMERS; ++i) {
            int arrivalTime = (int) arrivals.nextEventTime();
            int processTime = (int) processing.nextEventTime();
            Customer customer = new Customer(String.format("cust%03d", i), arrivalTime, processTime);
            waitingCustomers.add(customer);
        }
        finishedCustomers = new LinkedList<Customer>();
        this.clock = Clock.instance();
        clock.reset();
    }

    public void run() {
        while (!customersFinished()) {
            tick();
        }
    }

    private boolean customersFinished() {
        if (finishedCustomers.size() < MAX_CUSTOMERS) {
            return false;
        }
        for (Customer customer : finishedCustomers) {
            if (customer.getFinishTime() > clock.getTime()) {
                return false;
            }
        }
        return true;
    }

    public void tick() {
        clock.tick();
        Customer customer = waitingCustomers.peek();
        while (customer != null && customer.getArrivalTime() <= clock.getTime()) {
            finishedCustomers.add(waitingCustomers.remove());
            strategy.customerArrives(customer);
            customer = waitingCustomers.peek();
        }
        strategy.step();
    }

    public void printStatistics() {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int sum = 0;
        for (Customer customer : finishedCustomers) {
            int waitTime = customer.getWaitTime();
            if (min > waitTime) {
                min = waitTime;
            }
            if (max < waitTime) {
                max = waitTime;
            }
            sum += waitTime;
        }
        double average = (double) sum / finishedCustomers.size();
        double deltaSum = 0;
        for (Customer customer : finishedCustomers) {
            double delta = customer.getWaitTime() - average;
            deltaSum += (delta * delta);
        }
        double stdDev = Math.sqrt(deltaSum / finishedCustomers.size());
        System.out.printf("Strategy: %s\n", strategy.getClass().getName());
        System.out.printf("Min wait time: %d\n", min);
        System.out.printf("Max wait time: %d\n", max);
        System.out.printf("Avg wait time: %f\n", (double) sum / finishedCustomers.size());
        System.out.printf("StdDev: %f\n", stdDev);
        System.out.printf("Idle time: %d\n\n", strategy.getIdleTime());
    }

    public static void main(String[] args) {
        int numCashiers = 7;
        List<QueueStrategy> strategies = new ArrayList<QueueStrategy>();
        strategies.add(new RoundRobinStrategy(numCashiers));
        strategies.add(new ShortestLineStrategy(numCashiers));
        strategies.add(new FewestItemsStrategy(numCashiers));
        strategies.add(new SingleQueueStrategy(numCashiers));
        
        Random r = new Random();
        long seed1 = 31337; //r.nextLong();
        long seed2 = 55555; //r.nextLong();

        for (QueueStrategy strategy : strategies) {
            EventGenerator arrivals = new PoissonGenerator(0, 30, true);
            arrivals.setSeed(seed1);
            EventGenerator processing = new PoissonGenerator(10, 120, false);
            processing.setSeed(seed2);
            Simulator sim = new Simulator(strategy, arrivals, processing);
            sim.run();
            sim.printStatistics();
        }
    }
}
