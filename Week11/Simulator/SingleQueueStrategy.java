package Week11.Simulator;
import java.util.*;

/**
 * Write a description of class SingleQueueStrategy here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class SingleQueueStrategy extends AbstractQueueStrategy {

    private Queue<Customer> queue;
    
    public SingleQueueStrategy(int numCashiers) {
        super(numCashiers);
        queue = new LinkedList<Customer>();
    }

    /**
     * What should happen with a new customer arrives?
     */
    public void customerArrives(Customer customer) {
        queue.offer(customer);
    }
    
    @Override
    public void step() {
        for (Cashier cashier : cashiers) {
            if (!cashier.isBusy()) {
                cashier.customerArrives(queue.poll());
            }
        }
        super.step();
    }
}
