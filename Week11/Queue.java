package Week11;

/**
 * A collection designed for holding elements prior to processing.
 * Queues provide additional insertion, extraction, and inspection
 * operations. Each of these methods exists in two forms: one throws
 * an exception if the operation fails, the other returns a special
 * value (either null or false, depending on the operation). The
 * latter form of the insert operation is designed specifically
 * for use with capacity-restricted Queue implementations; in most
 * implementations, insert operations cannot fail.
 * 
 * @author Franklin University 
 * @version 2.0
 * 
 * @param E the type of element contained in the queue
 */
public interface Queue<E> {
    /**
     * Inserts the specified element into this queue if it is
     * possible to do so immediately without violating capacity
     * restrictions, returning true upon success and throwing
     * an IllegalStateException if no space is currently available.
     * 
     * @param element - the element to add
     * @returns true if the add was successful
     * @throws IllegalStateException - if the element cannot be
     *     added at this time due to capacity restrictions
     * @throws ClassCastException - if the class of the specified
     *     element prevents it from being added to this queue
     * @throws NullPointerException - if the specified element is
     *     null and this queue does not permit null elements
     * @throws IllegalArgumentException - if some property of this
     *     element prevents it from being added to this queue
     */
    boolean add(E element);
    
    /**
     * Retrieves, but does not remove, the head of this queue.
     * This method differs from peek only in that it throws an
     * exception if this queue is empty.
     * 
     * @returns the head of this queue
     * @throws NoSuchElementException - if the queue is empty
     */
    E element();
    
    /**
     * Inserts the specified element into this queue if it is
     * possible to do so immediately without violating capacity
     * restrictions. When using a capacity-restricted queue, this
     * method is generally preferable to add(E), which can fail
     * to insert an element only by throwing an exception.
     * 
     * @param element - the element to add
     * @returns true if the add was successful
     * @throws ClassCastException - if the class of the specified
     *     element prevents it from being added to this queue
     * @throws NullPointerException - if the specified element is
     *     null and this queue does not permit null elements
     * @throws IllegalArgumentException - if some property of this
     *     element prevents it from being added to this queue
     */
    boolean offer(E element);
    
    /**
     * Retrieves, but does not remove, the head of this queue,
     * or returns null if this queue is empty.
     * 
     * @returns the head of this queue, or null if the queue is empty
     */
    E peek();
    
    /**
     * Retrieves and removes the head of this queue, or returns
     * null if this queue is empty.
     * 
     * @returns the head of this queue, or null if the queue is empty
     */
    E poll();
    
    /**
     * Retrieves and removes the head of this queue. This method
     * differs from poll only in that it throws an exception if
     * this queue is empty.
     * 
     * @returns the head of this queue
     * @throws NoSuchElementException - if the queue is empty
     */
    E remove();
    
    /**
     * Returns true if the queue contains no elements.
     * 
     * @returns true if this queue contains no elements
     */
    boolean isEmpty();
    
    /**
     * Returns the number of elements in this queue.
     * 
     * @returns the numbe of elments in this queue.
     */
    int size();
}
