package Week05;

 



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class AlgorithmTimerTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class AlgorithmTimerTest {
    /**
     * Default constructor for test class AlgorithmTimerTest.
     */
    public AlgorithmTimerTest() {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
    }
    
    @Test
    public void testAlgorithm1() {
        AlgorithmTimer timer = new AlgorithmTimer();
        timer.addAlgorithm(new Algorithm1(), new IntervalGenerator(0, 100000, 10000));
        timer.timeAll();
    }
    
    @Test
    public void testAlgorithm2() {
        AlgorithmTimer timer = new AlgorithmTimer();
        timer.addAlgorithm(new Algorithm2(), new IntervalGenerator(0, 2000, 200));
        timer.timeAll();
    }
    
    @Test
    public void testAlgorithm3() {
        AlgorithmTimer timer = new AlgorithmTimer();
        timer.addAlgorithm(new Algorithm3(), new IntervalGenerator(0, 2000, 200));
        timer.timeAll();
    }
    
    @Test
    public void testAlgorithm4() {
        AlgorithmTimer timer = new AlgorithmTimer();
        timer.addAlgorithm(new Algorithm4(), new IntervalGenerator(0, 100000000, 10000000));
        timer.timeAll();
    }
    
    @Test
    public void testAlgorithm5() {
        AlgorithmTimer timer = new AlgorithmTimer();
        timer.addAlgorithm(new Algorithm5(), new IntervalGenerator(0, 100000, 10000));
        timer.timeAll();
    }
    
    @Test
    public void testAlgorithm6() {
        AlgorithmTimer timer = new AlgorithmTimer();
        timer.addAlgorithm(new Algorithm6(), new IntervalGenerator(0, 200, 20));
        timer.timeAll();
    }
    
    @Test
    public void testAlgorithm7() {
        AlgorithmTimer timer = new AlgorithmTimer();
        timer.addAlgorithm(new Algorithm7(), new IntervalGenerator(0, 2000, 200));
        timer.timeAll();
    }
    
    @Test
    public void testAlgorithm8() {
        AlgorithmTimer timer = new AlgorithmTimer();
        timer.addAlgorithm(new Algorithm8(), new IntervalGenerator(0, 100000, 10000));
        timer.timeAll();
    }
    
    @Test
    public void testAlgorithm9() {
        AlgorithmTimer timer = new AlgorithmTimer();
        timer.addAlgorithm(new Algorithm9(), new IntervalGenerator(0, 100000, 10000));
        timer.timeAll();
    }
    
    @Test
    public void testAlgorithm10() {
        AlgorithmTimer timer = new AlgorithmTimer();
        timer.addAlgorithm(new Algorithm10(), new IntervalGenerator(0, 2000, 200));
        timer.timeAll();
    }
    
    @Test
    public void testAlgorithm11() {
        AlgorithmTimer timer = new AlgorithmTimer();
        timer.addAlgorithm(new Algorithm11(), new IntervalGenerator(0, 100000, 10000));
        timer.timeAll();
    }
    
    @Test
    public void testAlgorithm12() {
        AlgorithmTimer timer = new AlgorithmTimer();
        timer.addAlgorithm(new Algorithm12(), new IntervalGenerator(10, 23, 1));
        timer.timeAll();
    }

}
