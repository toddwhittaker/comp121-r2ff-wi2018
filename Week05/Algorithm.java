package Week05;

 

public abstract class Algorithm {
    public abstract void execute(int n);

    public String getName() {
        return this.getClass().getName();
    }
    
    public String toString() {
        return getName();
    }
    
    public void wasteTime() {
        long startTime = System.nanoTime();
        while (true) {
            long endTime = System.nanoTime();
            if (endTime - startTime > 100) {
                break;
            }
        }
    }
}