package Week05;

 


public class Algorithm12 extends Algorithm {
    public void execute(int n) {
        if (n > 0) {
            execute(n-1);
            execute(n-1);
        }
        wasteTime();
    }
}
