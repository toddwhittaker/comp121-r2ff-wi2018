package Week05;

 

import java.util.List;
import java.util.ArrayList;

public class AlgorithmTimer {
    private List<Algorithm> algorithms;
    private List<Generator> generators;

    public AlgorithmTimer() {
        algorithms = new ArrayList<Algorithm>();
        generators = new ArrayList<Generator>();
    }

    private TimingResult time(Algorithm algorithm, int n) {
        System.gc();
        long startTime = System.nanoTime();
        algorithm.execute(n);
        long endTime = System.nanoTime();
        long millis = (endTime - startTime)/1000000;
        if (millis > 1000*10) {
            throw new RuntimeException("Algorithm " + algorithm.getName() + " took too long (" + (millis) + " ms) for n=" + n);
        }
        return new TimingResult(algorithm, n, millis);
    }
    
    public void addAlgorithm(Algorithm algorithm, Generator generator) {
        algorithms.add(algorithm);
        generators.add(generator);
    }
    
    public void timeAll() {
        for (int i = 0; i < algorithms.size(); ++i) {
            Generator gen = generators.get(i).reset();
            Algorithm algo = algorithms.get(i);
            
            System.out.println("Timing algorithm: " + algo.getName());
            System.out.println("n\ttime (ms)");
            for (int n : gen) {
                System.out.println(time(algo, n));
            }
        }
    }
}