package Week05;

 

public class TimingResult {
    public Algorithm algorithm;
    public int n;
    public long executionTime;
    
    public TimingResult(Algorithm algorithm, int n, long executionTime) {
        this.algorithm = algorithm;
        this.n = n;
        this.executionTime = executionTime;
    }
    
    public String toString() {
        return n + "\t" + executionTime;
    }
}