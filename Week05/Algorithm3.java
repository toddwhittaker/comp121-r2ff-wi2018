package Week05;

 


public class Algorithm3 extends Algorithm {
    public void execute(int n) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < i; ++j) {
                wasteTime();
            }
        }
    }
}
/*
 * 0 + 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 (when n = 10)
 * 9 + 8 + 7 + 6 + 5 + 4 + 3 + 2 + 1 + 0
 * --------------------------------------
 * 9 + 9 + 9 + 9 + 9 + 9 + 9 + 9 + 9 + 9
 * 
 * 10 * 9
 * ------- = 45
 *    2
 * 
 * n * (n - 1)
 * -----------
 *      2
 *      
 * .5n^2 - .5n
 * 
 * O(n^2)
 */
