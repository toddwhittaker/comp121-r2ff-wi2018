package Week05;

 

import java.util.Iterator;

public class IntervalGenerator implements Generator, Iterator<Integer> {
    private int start;
    private int stop;
    private int step;
    private int next;
    
    public IntervalGenerator(int start, int stop, int step) {
        this.start = start;
        this.stop = stop;
        this.step = step;
        reset();
    }
    
    public Generator reset() {
        this.next = start;
        return this;
    }
    
    public boolean hasNext() {
        return next <= stop;
    }
    
    public Integer next() {
        if (!hasNext()) {
            throw new IllegalStateException("No next");
        }
        Integer result = next;
        next += step;
        return result;
    }
    
    public Iterator<Integer> iterator() {
        return this;
    }
}