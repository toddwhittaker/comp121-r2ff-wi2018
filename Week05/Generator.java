package Week05;

 

import java.util.Iterator;

public interface Generator extends Iterable<Integer> {
    Generator reset();
}