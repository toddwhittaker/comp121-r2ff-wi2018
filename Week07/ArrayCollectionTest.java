package Week07;



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Iterator;

/**
 * The test class ArrayCollectionTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class ArrayCollectionTest extends AbstractCollectionTest {
    private ArrayCollection<Integer> coll;
    private int [] data = {3, 1, 4, 1, 5, 9, 2, 6, 5};   
    /**
     * Default constructor for test class ArrayCollectionTest.
     */
    public ArrayCollectionTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        super.setUp();
        coll = new ArrayCollection<Integer>();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        super.tearDown();
    }
    
    @Override
    protected <T> Collection<T> getCollectionToTest(Class<T> clazz) {
        return new ArrayCollection<T>();
    }
}
