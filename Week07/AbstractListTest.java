package Week07;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class AbstractListTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public abstract class AbstractListTest extends AbstractCollectionTest {
    /**
     * Default constructor for test class AbstractListTest.
     */
    public AbstractListTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        super.setUp();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        super.tearDown();
    }
    
    /**
     * Let the superclass get a collection to test from us.
     */
    @Override
    protected <T> Collection<T> getCollectionToTest(Class<T> clazz) {
        return getListToTest(clazz);
    }
    
    protected abstract <T> List<T> getListToTest(Class<T> clazz);
}
