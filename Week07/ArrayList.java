package Week07;

import java.util.List;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

/**
 * List is an extension of collection that adds the concepts of order,
 * indexed operations and bi-directional iteration.  The order of the
 * collection is guaranteed (unlike sets or collections).  Operations
 * may now take place at particular indices.  Finally, the list iterator
 * can move both forward and backward through the list and add elements
 * as well as remove them at iterator positions.
 * @param <E> the type of data to hold in the list
 * 
 * @author Todd A. Whittaker
 * @version 20171018
 */
public class ArrayList<E> extends AbstractList<E> {

    /** The backing store for our list. */
    private E [] data;

    /** How many of those data are in use? */
    private int size;

    /** The default capacity if none is specified. */
    private static final int DEFAULT_CAPACITY = 10;
    
    /**
     * Build an array list with a default size.
     */
    public ArrayList() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Build an array list with a given initial capacity.
     * @param initialCapacity
     */
    public ArrayList(int initialCapacity) {
        this.data = Utilities.newArray(data, initialCapacity);
        this.size = 0;
    }

    /**
     * Returns the number of elements in the collection.
     * @return the number of elements in the collection.
     */
    public int size() {
        return size;
    }

    /**
     * Make the data array hold more.
     */
    private void ensureCapacity(int requiredCapacity) {
        if (requiredCapacity > data.length) {
            int newCapacity = data.length * 2 + 1;
            if (requiredCapacity > newCapacity) {
                newCapacity = requiredCapacity;
            }
            E [] temp = Utilities.newArray(this.data, newCapacity);
            for (int i = 0; i < size; ++i) {
                temp[i] = data[i];
            }
            data = temp;
        }
    }
    
    /**
     * Returns an iterator over the elements in this collection. There are
     * no guarantees concerning the order in which the elements are returned
     * (unless this collection is an instance of some class that provides a
     * guarantee).
     * 
     * @return an iterator object.
     */
    public Iterator<E> iterator() {
        return listIterator();
    }

    /**
     * Returns a ListIterator over the elements in this list.  The starting
     * position for the list iterator will be the 0th element.
     * @return a ListIterator over the elements in this list.
     */
    public ListIterator<E> listIterator() {
        return listIterator(0);
    }
    
    /**
     * Replaces the element at the specified index with the parameter
     * element.  Returns the element which was replaced.
     * @param index the index of the object to be replaced
     * @param obj the object to use as a replacement
     * @return the object originally at the location
     * @throws IndexOutOfBoundsException if index < 0 or index >= size()
     */
    public E set(int index, E obj) {
        return null;
    }

    /**
     * Inserts all the elements of <tt>coll</tt> into this collection at
     * the specified location.  If both this collection and the parameter
     * are the same collection, then the operational behavior is undefined
     * (i.e. bad things can happen).
     * @param index the location at which to insert
     * @param coll the collection from which to draw elements for addition.
     * @return true when this collection is modified as a result.
     */
    public boolean addAll(int index, Collection<? extends E> coll) {
        /*# This is a HW 7 problem. :( */
        return false;
    }

    /**
     * Returns a ListIterator over the elements in this list starting at
     * the given position.
     * @param index the starting index for iteration
     * @throws IndexOutOfBoundsException if the index < 0 or index > size()
     */
    public ListIterator<E> listIterator(int index) {
        return new ArrayListIterator(index);
    }

    /**
     * Create a sublist of this list between the given indices.  The sublist
     * is not a copy of the elements of this list, but rather a restricted
     * view on them.  Thus, any insertions, deletions, or replacements
     * of elements in the sublist will affect the original list.
     * @param fromIndex the starting index (inclusive) of the sublist
     * @param toIndex the ending index (exclusive) of the sublist
     * @return a restricted view on this list
     * @throws IndexOutOfBoundsException if either index is < 0 or > size.
     */
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }

    /**
     * An object that abstracts the idea of iterator (both forward and
     * backward) over a list of objects.  It also allows the replacement
     * of elements on the fly, and the extraction of the iterator position
     * in terms of an index value between elements (<tt>0</tt> being
     * <em>before</em>the 0th element, and <tt>n+1</tt> being <em>after</em>
     * the nth element).
     * @param <E> the type of data returned
     * 
     * @author Todd A. Whittaker
     * @version 2005-09
     */
    private class ArrayListIterator implements ListIterator<E> {
        
        private int cursor;
        private int priorCursor; // index of the last thing returned by next() or previous()
        private static final int BAD_CURSOR = -1;
        
        public ArrayListIterator(int startIndex) {
            if (startIndex < 0 || startIndex > size()) {
                throw new IndexOutOfBoundsException();
            }
            cursor = startIndex;
            priorCursor = BAD_CURSOR;
        }

        /**
         * Returns true when the iterator has more data to return.
         * @return true if the iterator has more data
         */
        public boolean hasNext() {
            return cursor < size();
        }

        /**
         * Returns the next element from the iteration.
         * @return the next data element
         */
        public E next() {
            if (!hasNext()) {
                throw new java.util.NoSuchElementException();
            }
            priorCursor = cursor;
            return data[cursor++];
        }

        /**
         * Removes the previously returned element from the iteration.
         * @throws IllegalStateException when next is not called
         * immediately before remove.
         */
        public void remove() {
            if (priorCursor == BAD_CURSOR) {
                throw new IllegalStateException();
            }
            for (int i = priorCursor; i < size() - 1; ++i) {
                data[i] = data[i+1];
            }
            cursor = priorCursor;
            data[--size] = null;
            priorCursor = BAD_CURSOR;
        }

        /**
         * Inserts the specified element into the list at the present iterator
         * position.  The insertion point is before the element that would be
         * returned by a call to <tt>next</tt> and after the element that would
         * be returned by a call to <tt>previous</tt>.  After adding the element,
         * a call to <tt>previous</tt> will return the newly inserted element,
         * but a call to <tt>next</tt> will return the element immediately
         * after the one inserted.
         * @param obj the element to insert
         * @throws ConcurrentModificationException
         */
        public void add(E obj) {
            ensureCapacity(size + 1);
            for (int i = size(); i > cursor; --i) {
                data[i] = data[i - 1];
            }
            data[cursor++] = obj;
            ++size;
        }

        /**
         * Returns true when the iterator has more data to return in the
         * reverse direction.
         * @return true if the iterator has more data
         */
        public boolean hasPrevious() {
            return cursor > 0;
        }

        /**
         * Returns the index of the element that would be returned should
         * a subsequent call to <tt>next</tt> be made.
         * @return the index of the next element
         */
        public int nextIndex() {
            return cursor;
        }

        /**
         * Returns the element from the iteration when traversing in the
         * reverse direction.
         * @return the previous data element
         */
        public E previous() {
            if (!hasPrevious()) {
                throw new java.util.NoSuchElementException();
            }
            priorCursor = --cursor;
            return data[cursor];
        }

        /**
         * Returns the index of the element that would be returned should
         * a subsequent call to <tt>previous</tt> be made.
         * @return the index of the previous element
         */
        public int previousIndex() {
            return cursor - 1;
        }

        /**
         * Replaces the last element returned by either <tt>next</tt> or
         * <tt>previous</tt>.  Cannot be called immediately after <tt>add</tt>
         * or <tt>remove</tt>.
         * @param obj the object with which to replace the one returned
         * earlier.
         */
        public void set(E obj) {
            /*# This is a HW 7 problem. :( */
        }

    }
}
