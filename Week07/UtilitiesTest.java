package Week07;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class UtilitiesTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class UtilitiesTest {
    /**
     * Default constructor for test class UtilitiesTest.
     */
    public UtilitiesTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.setUp();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.tearDown();
    }

    /**
     * General purpose test for resizing arrays.
     * @param <E> Type of data in the array
     * @param orig Array of data from which to copy
     * @param newSize the size of the new array to make
     */
    private <E> void resizeArrayHelper(E [] orig, int newSize) {
        E [] result = Utilities.resizeArray(orig, newSize);
        assertEquals(newSize, result.length);

        int i;
        for (i = 0; orig != null && i < orig.length && i < result.length; ++i) {
            assertEquals(orig[i], result[i]);
        }
        for ( ; i < result.length; ++i) {
            assertNull(result[i]);
        }
    }

    /**
     * Test growing an array.
     */
    @Test
    public void testResizeArrayGrow() {
        resizeArrayHelper(
            new Integer [] {1, 2, 3, 4, 5, 6, 7, 8},
            10);
    }

    /**
     * Test growing from null array.
     */
    @Test
    public void testResizeArrayNull() {
        Integer [] arr = null;
        resizeArrayHelper(arr, 10);
    }

    /**
     * Test shrinking an array.
     */
    @Test
    public void testResizeArrayShrink() {
        resizeArrayHelper(
            new Integer [] {1, 2, 3, 4, 5, 6, 7, 8},
            4);
    }

    /**
     * Test that nullSafeEquals works.
     */
    @Test
    public void testNullSafeEquals() {
        assertTrue(Utilities.nullSafeEquals(null, null));
        assertFalse(Utilities.nullSafeEquals(null, "testme"));
        assertFalse(Utilities.nullSafeEquals("testme", null));
        assertTrue(Utilities.nullSafeEquals("testme", "testme"));
    }
}