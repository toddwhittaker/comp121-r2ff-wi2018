package Week07;

import java.util.Collection;
import java.util.Iterator;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class AbstractCollectionTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public abstract class AbstractCollectionTest {
    
    private Collection<Integer> coll;
    protected int [] data = {3, 1, 4, 1, 5, 9, 2, 6};

    /**
     * Default constructor for test class AbstractCollectionTest.
     */
    public AbstractCollectionTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.setUp();
        coll = getCollectionToTest(Integer.class);
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.tearDown();
    }
    
    protected abstract <T> Collection<T> getCollectionToTest(Class<T> clazz);
    
    protected void fill(Collection<Integer> coll, int [] data) {
        for (int num : data) {
            coll.add(num);
        }
    }

    @Test
    public void testConstruction() {
        assertNotNull(coll);
        assertTrue(coll.isEmpty());
        assertEquals(0, coll.size());
    }
    
    @Test
    public void testAdd1() {
        assertTrue(coll.add(3));
        assertFalse(coll.isEmpty());
        assertEquals(1, coll.size());
    }
    
    @Test
    public void testAdd2() {
        fill(coll, data);
        assertFalse(coll.isEmpty());
        assertEquals(data.length, coll.size());
    }
    
    @Test
    public void testContains1() {
        fill(coll, data);
        assertTrue(coll.contains(3));
        assertTrue(coll.contains(5));
        assertFalse(coll.contains(99));
    }
    
    @Test
    public void testContains2() {
        fill(coll, data);
        assertFalse(coll.contains(null));
        coll.add(null);
        assertTrue(coll.contains(null));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testBadConstructor() {
        coll = new ArrayCollection(-5);
    }
    
    @Test
    public void testIterator1() {
        fill(coll, data);
        Iterator<Integer> itr = coll.iterator();
        assertNotNull(itr);
        assertTrue(itr.hasNext());
    }
    
    @Test
    public void testIterator2() {
        fill(coll, data);
        Iterator<Integer> itr = coll.iterator();
        int i = 0;
        while (itr.hasNext()) {
            assertEquals(data[i], itr.next().intValue());
            ++i;
        }
        assertEquals(i, data.length);
    }
    
    @Test
    public void testIteratorRemove() {
        fill(coll, data);
        Iterator<Integer> itr = coll.iterator();
        while (itr.hasNext()) {
            Integer num = itr.next();
            if (num == 1) {
                itr.remove();
            }
        }
        assertEquals(data.length - 2, coll.size());
        assertFalse(coll.contains(1));
    }
    
    @Test
    public void testRemove() {
        fill(coll, data);
        assertFalse(coll.remove(99));
        assertTrue(coll.remove(3));
        assertFalse(coll.contains(3));
        assertEquals(data.length - 1, coll.size());
    }
    
    @Test
    public void testClear() {
        fill(coll, data);
        coll.clear();
        assertTrue(coll.isEmpty());
    }

    @Test
    public void testToArray1() {
        fill(coll, data);
        Object [] result = coll.toArray();
        assertEquals(data.length, result.length);
        for (int i = 0; i < data.length; ++i) {
            assertEquals(data[i], ((Integer)result[i]).intValue());
        }
    }
    
    @Test
    public void testToArray2() {
        fill(coll, data);
        Integer [] initial = new Integer[data.length + 4];
        for (int i = 0; i < initial.length; ++i) {
            initial[i] = -99;
        }
        Integer [] result = coll.toArray(initial);
        assertSame(initial, result);
        int i = 0;
        while (i < data.length) {
            assertEquals(data[i], result[i].intValue());
            ++i;
        }
        while (i < result.length) {
            assertNull(result[i]);
            ++i;
        }
    }
    
    @Test
    public void testToArray3() {
        fill(coll, data);
        Integer [] initial = new Integer[data.length - 2];
        for (int i = 0; i < initial.length; ++i) {
            initial[i] = -99;
        }
        Integer [] result = coll.toArray(initial);
        assertNotSame(initial, result);
        assertEquals(data.length, result.length);
        int i = 0;
        while (i < data.length) {
            assertEquals(data[i], result[i].intValue());
            ++i;
        }
    }
}
