package Week07;

import java.util.List;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class ArrayListTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class ArrayListTest extends AbstractListTest {
    
    private List<Integer> list;
    
    /**
     * Default constructor for test class ArrayListTest.
     */
    public ArrayListTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        super.setUp();
        list = getListToTest(Integer.class);
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        super.tearDown();
    }
    
    @Override
    protected <T> List<T> getListToTest(Class<T> clazz) {
        return new ArrayList<T>();
    }
    
    @Test
    public void testGrowingLarge() {
        list = new ArrayList<Integer>(10);
        for (int i = 0; i < 100; ++i) {
            assertTrue(list.add(i));
        }
        assertEquals(100, list.size());
    }
    
    @Test
    public void testAddAtIterator() {
        int targetLocation = 2;
        fill(list, data);
        ListIterator<Integer> itr = list.listIterator(targetLocation);
        itr.add(99);
        assertEquals(data.length + 1, list.size());
        int index = 0;
        System.out.println(list);
        Iterator<Integer> itr2 = list.iterator();
        while (itr2.hasNext()) {
            Integer num = itr2.next();
            if (index == targetLocation) {
                assertEquals(99, num.intValue());
            }
            ++index;
        }
    }
    
    @Test
    public void testAddAtIndex() {
        fill(list, data);
        list.add(2, 99);
        assertTrue(list.toString().startsWith("3, 1, 99, 4,"));
        assertEquals(data.length + 1, list.size());
    }
    
    @Test
    public void testNextIndex() {
        fill(list, data);
        ListIterator<Integer> itr = list.listIterator(3);
        assertEquals(3, itr.nextIndex());
    }
    
    @Test
    public void testIteratingBackwards() {
        fill(list, data);
        ListIterator<Integer> itr = list.listIterator(list.size());
        int index = data.length - 1;
        while (itr.hasPrevious()) {
            assertEquals(index, itr.previousIndex());
            Integer num = itr.previous();
            assertEquals(data[index], num.intValue());
            --index;
        }
        assertEquals(-1, index);
    }
    
    @Test
    public void testRemovingBackwards() {
        fill(list, data);
        ListIterator<Integer> itr = list.listIterator(list.size());
        while (itr.hasPrevious()) {
            itr.previous();
            itr.remove();
        }
        assertEquals(0, list.size());
    }
}
