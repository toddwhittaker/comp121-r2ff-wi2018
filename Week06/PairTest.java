package Week06;



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class PairTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class PairTest {
    /**
     * Default constructor for test class PairTest.
     */
    public PairTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.setUp();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.tearDown();
    }
    
    /**
     * A sample test case.
     *
     * Process:
     *  1. Create an object to test
     *  2. Assert something about the initial object state
     *  3. Call a method
     *  4. Assert something about the return value
     *     or the state of the object after the call
     */
    @Test
    public void testFeature1() {
        Pair<Comparable, String> p1 = new Pair<>("cat", "el gato");
        Pair<String, String> p2 = new Pair<>("table", "la mesa");
        assertEquals("cat", p1.getFirst());
        assertEquals("la mesa", p2.getSecond());
    }
    
    @Test
    public void testFeature2() {
        Pair<Integer, String> p = new Pair<>(1, "one");        
        assertEquals(Integer.valueOf(1), p.getFirst());
    }
}
