package Week06;



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Iterator;

/**
 * The test class ArrayCollectionTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class ArrayCollectionTest {
    private ArrayCollection<Integer> coll;
    private int [] data = {3, 1, 4, 1, 5, 9, 2, 6, 5};   
    /**
     * Default constructor for test class ArrayCollectionTest.
     */
    public ArrayCollectionTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.setUp();
        coll = new ArrayCollection<Integer>();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.tearDown();
    }
    
    @Test
    public void testConstruction() {
        assertNotNull(coll);
        assertTrue(coll.isEmpty());
        assertEquals(0, coll.size());
    }
    
    @Test
    public void testAdd1() {
        assertTrue(coll.add(3));
        assertFalse(coll.isEmpty());
        assertEquals(1, coll.size());
    }
    
    private void fill(Collection<Integer> coll) {
        for (int num : data) {
            coll.add(num);
        }
    }
    
    @Test
    public void testAdd2() {
        fill(coll);
        assertFalse(coll.isEmpty());
        assertEquals(data.length, coll.size());
    }
    
    @Test
    public void testContains1() {
        fill(coll);
        assertTrue(coll.contains(3));
        assertTrue(coll.contains(5));
        assertFalse(coll.contains(99));
    }
    
    @Test
    public void testContains2() {
        fill(coll);
        assertFalse(coll.contains(null));
        coll.add(null);
        assertTrue(coll.contains(null));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testBadConstructor() {
        coll = new ArrayCollection(-5);
    }
    
    @Test
    public void testIterator1() {
        fill(coll);
        Iterator<Integer> itr = coll.iterator();
        assertNotNull(itr);
        assertTrue(itr.hasNext());
    }
    
    @Test
    public void testIterator2() {
        fill(coll);
        Iterator<Integer> itr = coll.iterator();
        int i = 0;
        while (itr.hasNext()) {
            assertEquals(data[i], itr.next().intValue());
            ++i;
        }
        assertEquals(i, data.length);
    }
    
    @Test
    public void testIteratorRemove() {
        fill(coll);
        Iterator<Integer> itr = coll.iterator();
        while (itr.hasNext()) {
            Integer num = itr.next();
            if (num == 1) {
                itr.remove();
            }
        }
        assertEquals(data.length - 2, coll.size());
        assertFalse(coll.contains(1));
    }
    
    @Test
    public void testRemove() {
        fill(coll);
        assertFalse(coll.remove(99));
        assertTrue(coll.remove(3));
        assertFalse(coll.contains(3));
        assertEquals(data.length - 1, coll.size());
    }
    
    @Test
    public void testClear() {
        fill(coll);
        coll.clear();
        assertTrue(coll.isEmpty());
    }
}
