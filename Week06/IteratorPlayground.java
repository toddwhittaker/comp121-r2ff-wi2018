package Week06;

import java.util.*;

/**
 * Write a description of class IteratorPlayground here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class IteratorPlayground {
    public static void main(String [] args) {
        Collection<Integer> c = new ArrayList<>();
        for (int i = 0; i < 100; ++i) {
            c.add(i*2);
        }
        System.out.println(c);
        
        Iterator<Integer> itr = c.iterator();
        while (itr.hasNext()) {
            Integer num = itr.next();
            if (num % 3 != 0) {
                itr.remove();
            }
        }
       
        System.out.println(c);
    }
}
