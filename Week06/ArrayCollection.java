package Week06;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Write a description of class ArrayCollection here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ArrayCollection<E> extends AbstractCollection<E> {

    private int size;
    private E [] data;

    public ArrayCollection() {
        this(10);
    }

    public ArrayCollection(int initialSize) {
        if (initialSize <= 0) {
            throw new IllegalArgumentException("Must have a postive initial size");
        }
        this.size = 0;
        data = (E[])(new Object[initialSize]);
    }

    /**
     * Returns the number of elements in the collection.
     * @return the number of elements in the collection.
     */
    public int size() {
        return this.size;
    }

    /**
     * Inserts an element into a collection.  Returns true if the collection
     * is changed as a result of the operation, and false otherwise.
     * @param obj the object to add to the collection
     * @return true if the collection is altered
     */
    @Override
    public boolean add(E element) {
        if (size < data.length) {
            data[size] = element;
            ++size;
            return true;
        }
        return false;
    }


    /**
     * Removes all elements from the collection.  The collection will
     * be empty after calling this method.
     */
    @Override
    public void clear() {
        for (int i = 0; i < size; ++i) {
            data[i] = null;
        }
        size = 0;
    }

    /**
     * Searches this collection to determine if every element of <tt>coll</tt>
     * exists in this collection.  Comparison is based on the same algorithm
     * as for <tt>contains</tt>.
     * @param coll the collection to be checked for containment in this on
     * @return true if all elements of <tt>coll</tt> also exist in this.
     */
    public boolean containsAll(Collection<?> coll) {
        /*# TODO: this is a Homework problem :( */
        return false;
    }

    /**
     * Retains all the elements in this collection that match those in
     * the parameter collection <tt>coll</tt>.  That is, it will remove
     * all elements in this collection that have no match in <tt>coll</tt>.
     * @parma coll the collection of elements to be matched against
     * @return true if this collection is altered as a result of the call.
     */
    public boolean retainAll(Collection<?> coll) {
        /*# TODO: this is a Homework problem :( */
        return false;
    }

    /**
     * Compares the parameter object <tt>obj</tt> against this collection
     * for equality.  Care should be taken to ensure symmetry, transitivity,
     * and reflexivity properties of equality.  That is: if
     * <tt>a.equals(b)</tt> is true then <tt>b.equals(a)</tt> should also be
     * true (symmetry); if <tt>a.equals(b)</tt> and <tt>b.equals(c)</tt> then
     * <tt>a.equals(c)</tt> should be true (transitivity); and also
     * <tt>a.equals(a)</tt> should also be true.
     * 
     * The contract between <tt>equals</tt> and <tt>hashCode</tt> should
     * also be kept.  That is, if <tt>a.equals(b)</tt> is true then
     * <tt>a.hashCode() == b.hashCode()</tt> should also be true.  The
     * reverse is not true (equal hash codes do not imply object equality).
     * 
     * A typical way to implement <tt>equals</tt> would be do do a value
     * comparison of the contents of the collection.
     * 
     * @param obj the object against which to compare this collection.
     * @return true if the object is equal to this collection.
     */
    public boolean equals(Object obj) {
        return false;
    }

    /**
     * Returns the hash code value for this collection.  A typical
     * implementation would be to somehow aggregate the hash codes of the
     * individual elements of the collection.  Be sure that hashCode is
     * also overridden if equals is overridden.
     * @return the hash code of the collection.
     */
    public int hashCode() {
        return 0;
    }

    /**
     * Creates and returns an array containing the data from this collection.
     * If this collection preserves order, then the order in the array
     * will be the same as the order of the collection.  The array returned
     * will always be safe to be modified (i.e. it will be different from
     * the underlying collection's array if the collection is array-backed).
     * @return an array containing references to the collections elements.
     */
    public Object [] toArray() {
        return null;
    }

    /**
     * Returns an array containing the data from this collection.  If the
     * parameter array is large enough to hold the collection, then it is
     * filled and returned.  Otherwise, a new array of the same type is
     * allocated, filled, and returned.  If this collection preserves order
     * then the order in the array will be the same as the order of the
     * collection. The array returned will always be safe to be modified
     * (i.e. it will be different from the underlying collection's array
     * if the collection is array-backed). The last element of the array
     * will be set to null if there is sufficient room.
     * 
     * @param a - the array to fill with the elements of this collection. If
     * the array is large enough, then the elements [0, size()-1] will be
     * overwritten. Otherwise, a new array will be constructed.
     * @return an array containing references to the collections elements.
     */
    public <E> E[] toArray(E[] a) {
        return null;
    }

    /**
     * Returns an iterator over the elements in this collection. There are
     * no guarantees concerning the order in which the elements are returned
     * (unless this collection is an instance of some class that provides a
     * guarantee).
     * 
     * @return an iterator object.
     */
    public Iterator<E> iterator() {
        return new ArrayCollectionIterator();
    }

    private class ArrayCollectionIterator implements Iterator<E> {

        private int index;
        private boolean nextCalled;

        public ArrayCollectionIterator() {
            index = 0;
            nextCalled = false;
        }

        public boolean hasNext() {
            return index < size();
        }

        public E next() {
            if (!hasNext()) {
                throw new NoSuchElementException("Index out of bounds");
            }
            nextCalled = true;
            return data[index++];
        }

        public void remove() {
            if (!nextCalled) {
                throw new IllegalStateException("No prior call to next()");
            }
            // remove the thing at index - 1.
            for (int i = index - 1; i < size() - 1; ++i) {
                data[i] = data[i+1];
            }
            --index;
            data[--size] = null;
            nextCalled = false;
        }
    }
}
