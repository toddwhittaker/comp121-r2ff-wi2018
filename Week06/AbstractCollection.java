package Week06;

import java.util.Collection;
import java.util.Iterator;

/**
 * Write a description of class AbstractCollection here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public abstract class AbstractCollection<E> implements Collection<E> {

    /**
     * Returns true if the collection contains no elements.
     * @return true if the collection is empty.
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Searches the collection to determine if the collection contains
     * an element that matches the specified object.  If the specified
     * object is null, it will look for a null in the collection.  Otherwise
     * it will use the <tt>equals</tt> method of the given object to 
     * determine equality.  That is, <tt>o == null ? element == null : 
     * o.equals(element)</tt>.
     * @param obj the object to match
     * @return true if the collection contains a matching element
     */
    public boolean contains(Object obj) {
        Iterator<E> itr = this.iterator();
        while (itr.hasNext()) {
            if (Utilities.nullSafeEquals(obj, itr.next())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Removes the first element of this collection matching the parameter
     * object <tt>obj</tt>.  If the colleciton is altered as a result of
     * the operation, <tt>remove</tt> returns <tt>true</tt>.
     * @param obj the object to match
     * @return true if the collection is altered, false otherwise.
     */
    public boolean remove(Object obj) {
        Iterator<E> itr = this.iterator();
        while (itr.hasNext()) {
            if (Utilities.nullSafeEquals(obj, itr.next())) {
                itr.remove();
                return true;
            }
        }
        return false;
    }

    /**
     * Removes all elements from the collection.  The collection will
     * be empty after calling this method.
     */
    @Override
    public void clear() {
        Iterator<E> itr = this.iterator();
        while (itr.hasNext()) {
            itr.next();
            itr.remove();
        }
    }

    /**
     * Inserts all the elements of <tt>coll</tt> into this collection.  If
     * both this collection and the parameter are the same collection, then
     * the operational behavior is undefined (i.e. bad things can happen).
     * @param coll the collection from which to draw elements for addition.
     * @return true when this collection is modified as a result.
     */
    public boolean addAll(Collection<? extends E> coll) {
        int originalSize = size();
        for (E element : coll) {
            this.add(element);
        }
        return originalSize != size();
    }


    /**
     * Removes all elements in this collection that match those in the
     * parameter collection <tt>coll</tt>.  When the operation completes,
     * the two collections will be disjoint.
     * @param coll the collection of elements to be removed from this one.
     * @return true if this collection is altered as a result of the call.
     */
    public boolean removeAll(Collection<?> coll) {
        int originalSize = size();
        for (Object obj : coll) {
            while (this.remove(obj)) {
                // empty;
            }
        }
        return originalSize != size();
    }

}
