package Week06;


/**
 * Write a description of class Pair here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Pair<FRED, BARNEY> {
    private FRED first;
    private BARNEY second;
    
    public Pair(FRED first, BARNEY second) {
        this.first = first;
        this.second = second;
    }
    
    public FRED getFirst() {
        return this.first;
    }
    
    public BARNEY getSecond() {
        return this.second;
    }
}
