package Week10;

import java.util.List;

public class VirtualMachine {

    private int [] vars;
    private Stack<Integer> stack;
    
    public VirtualMachine() {
        vars = new int[26];
        stack = new ArrayListStack<Integer>();
    }

    public void execute(List<String> commands) {
        for (String command : commands) {
            execute(command);
        }
        if (!stack.empty()) {
            throw new RuntimeException("malformed program");
        }
    }

    private void execute(String command) {
        if (command.startsWith("push ")) {
            String value = command.substring(5);
            try {
                stack.push(Integer.parseInt(value));
            } catch (NumberFormatException e) {
                char var = value.charAt(0);
                stack.push(getVariable(var));
            }
        } else if (command.startsWith("pop ")) {
            setVariable(command.charAt(4), stack.pop());
        } else {
            int op2 = stack.pop();
            int op1 = stack.pop();
            
            if ("add".equals(command)) {
                stack.push(op1 + op2);
            } else if ("sub".equals(command)) {
                stack.push(op1 - op2);
            } else if ("mul".equals(command)) {
                stack.push(op1 * op2);
            } else if ("div".equals(command)) {
                stack.push(op1 / op2);
            } else {
                throw new RuntimeException("Unknown operator");
            }
        }
    }

    public int getVariable(char var) {
        return vars[var - 'a'];
    }

    private void setVariable(char var, int value) {
        vars[var - 'a'] = value;
    }
    
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (char var = 'a'; var <= 'z'; ++var) {
            int num = getVariable(var);
            if (num != 0) {
                builder.append(String.format("%c = %d\n", var, num));
            }
        }
        return builder.toString();
    }
    
    public static void main(String [] args) {
        Assembler asm = new Assembler();
        VirtualMachine vm = new VirtualMachine();
        
        asm.processLine("y = ( 3 + 7 ) * ( ( ( 10 ) ) ) - 8 / 2");
        asm.processLine("x = 5 - y" );
        asm.processLine("z = x * ( 3 + y )");
        
        for (String cmd : asm.getCommands()) {
            System.out.println(cmd);
        }
        
        vm.execute(asm.getCommands());
        
        System.out.println(vm);
    }
}
