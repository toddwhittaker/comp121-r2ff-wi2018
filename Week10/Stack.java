package Week10;

/**
 * The Stack class represents a last-in-first-out (LIFO) stack
 * of objects. The usual push and pop operations are provided,
 * as well as a method to peek at the top item on the stack, a
 * method to test for whether the stack is empty.
 * 
 * When a stack is first created, it contains no items.
 * 
 * @param <E> the element type that the stack holds
 */
public interface Stack<E> {
    
    /**
     * Tests if this stack is empty.
     * 
     * @return true if and only if the stack contains no items;
     * false otherwise
     */
    boolean empty();
    
    /**
     * Looks at the object at the top of this stack without
     * removing it from the stack.
     * 
     * @return the object at the top of this stack (the last
     * item inserted).
     * 
     * @throws EmptyStackException - if this stack is empty.
     */
    E peek();
    
    /**
     * Removes the object at the top of this stack and returns
     * that object as the value of this function.
     * 
     * @return the object at the top of this stack (the last
     * item inserted).
     * 
     * @throws EmptyStackException - if this stack is empty.
     */
    E pop();
    
    /**
     * Pushes an element onto the top of this stack.
     * 
     * @param element - the item to be pushed onto this stack.
     * 
     * @return the stack
     */
    E push(E element);
}
