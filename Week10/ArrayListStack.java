package Week10;


import java.util.ArrayList;
import java.util.EmptyStackException;

public class ArrayListStack<E> implements Stack<E> {

    private ArrayList<E> theStack;
    
    public ArrayListStack() {
        theStack = new ArrayList<E>();
    }
    
    /**
     * Tests if this stack is empty.
     * 
     * @return true if and only if the stack contains no items;
     * false otherwise
     */
    public boolean empty() {
        return theStack.isEmpty();
    }

    /**
     * Looks at the object at the top of this stack without
     * removing it from the stack.
     * 
     * @return the object at the top of this stack (the last
     * item inserted).
     * 
     * @throws EmptyStackException - if this stack is empty.
     */
    public E peek() {
        if (empty()) {
            throw new EmptyStackException();
        }
        return theStack.get(theStack.size() - 1);
    }

    /**
     * Removes the object at the top of this stack and returns
     * that object as the value of this function.
     * 
     * @return the object at the top of this stack (the last
     * item inserted).
     * 
     * @throws EmptyStackException - if this stack is empty.
     */
    public E pop() {
        E result = peek();
        theStack.remove(theStack.size() - 1);
        return result;
    }

    /**
     * Pushes an element onto the top of this stack.
     * 
     * @param element - the item to be pushed onto this stack.
     * 
     * @return the element argument
     */
    public E push(E element) {
        theStack.add(element);
        return element;
    }
}
