package Week10;

import java.util.List;
import java.util.ArrayList;

public class Assembler {
    private List<String> commands;
    private Stack<String> stack;

    public Assembler() {
        commands = new ArrayList<String>();
        stack = new ArrayListStack<String>();
    }

    public void processLine(String line) {
        String [] tokens = line.split(" ");

        for (int i = 2; i < tokens.length; ++i) {
            processToken(tokens[i]);
        }

        while (!stack.empty() ){
            String op = stack.pop();
            commands.add(opToCommand(op));
        }
        commands.add("pop " + tokens[0]);
    }

    private void processToken(String token) {
        try {
            int num = Integer.parseInt(token);
            commands.add("push " + num);
            return;
        } catch (NumberFormatException e) {
            // do nothing;
        }        
        // we know token is ()+-/* or variable
        switch (token.charAt(0)) {
            case '(' :
                stack.push(token);
                break;
            case ')' :
                while (true) {
                    String op = stack.pop();
                    if ("(".equals(op)) {
                        break;
                    }
                    commands.add(opToCommand(op));
                }
                break;
            case '+' :
            case '-' :
            case '/' :
            case '*' :
                while (!stack.empty() && precedence(stack.peek()) >= precedence(token)) {
                    String op = stack.pop();
                    commands.add(opToCommand(op));
                }
                stack.push(token);
                break;
            default :
                commands.add("push " + token);
                break;
        }
    }

    private static int precedence(String token) {
        switch (token.charAt(0)) {
            case '+' : return 0;
            case '-' : return 0;
            case '/' : return 1;
            case '*' : return 1;
        }
        return -1;
    }
    
    private static String opToCommand(String op) {
        switch (op.charAt(0)) {
            case '+' : return "add";
            case '-' : return "sub";
            case '/' : return "div";
            case '*' : return "mul";
        }
        throw new RuntimeException("oops. unknown token " + op);
    }
    
    public List<String> getCommands() {
        return commands;
    }

}
