package Week10;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class AssemblerTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class AssemblerTest {
    Assembler asm;

    /**
     * Default constructor for test class AssemblerTest.
     */
    public AssemblerTest() {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        asm = new Assembler();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
    }

    @Test
    public void testSimple1() {
        asm.processLine("x = 3");
        assertEquals("push 3", asm.getCommands().get(0));
        assertEquals("pop x", asm.getCommands().get(1));
    }

    @Test
    public void testAddExpression() {
        asm.processLine("x = 3 + 7");
        assertEquals("push 3", asm.getCommands().get(0));
        assertEquals("push 7", asm.getCommands().get(1));
        assertEquals("add", asm.getCommands().get(2));
        assertEquals("pop x", asm.getCommands().get(3));
    }

    @Test
    public void testParens() {
        asm.processLine("x = ( 3 + 7 ) * 2");
        String [] expected = {"push 3", "push 7", "add", "push 2", "mul", "pop x"};
        for (int i = 0; i < expected.length; ++i) {
            assertEquals(expected[i], asm.getCommands().get(i));
        }
    }

    @Test
    public void testComplex() {
        asm.processLine("x = 3 + 4 - 7 * 2 + 6 / 3 + y");
        String [] expected = {"push 3", "push 4", "add", "push 7", "push 2",
                "mul", "sub", "push 6", "push 3", "div", "add", "push y", "add", "pop x" };
        for (int i = 0; i < expected.length; ++i) {
            assertEquals(expected[i], asm.getCommands().get(i));
        }
    }
}
