package Week10;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.EmptyStackException;

public class ArrayListStackTest {
    private ArrayListStack<Integer> stack;

    /**
     * Default constructor for test class ArrayListStackTest.
     */
    public ArrayListStackTest() {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        stack = new ArrayListStack<Integer>();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
    }

    @Test
    public void testConstruction() {
        assertNotNull(stack);
        assertTrue(stack.empty());
    }

    @Test
    public void testPush() {
        assertEquals(5, stack.push(5).intValue());
        assertFalse(stack.empty());
    }

    @Test
    public void testPeek1() {
        boolean didThrow = false;
        try {
            stack.peek();
        } catch (EmptyStackException e) {
            didThrow = true;
        }
        assertTrue(didThrow);
    }

    @Test
    public void testPeek2() {
        stack.push(5);
        assertEquals(5, stack.peek().intValue());
        assertFalse(stack.empty());
        stack.push(99);
        assertEquals(99, stack.peek().intValue());
    }

    @Test
    public void testPop1() {
        stack.push(5);
        assertEquals(5, stack.pop().intValue());
        assertTrue(stack.empty());
    }

    @Test
    public void testPop2() {
        for (int i = 0; i < 100; ++i) {
            stack.push(i);
        }
        for (int i = 0; i < 100; ++i) {
            assertEquals(99-i, stack.pop().intValue());
            if (99-i > 0) {
                assertFalse(stack.empty());
            } else {
                assertTrue(stack.empty());
            }
        }
    }

    @Test
    public void testPop3() {
        boolean didThrow = false;
        try {
            stack.pop();
        } catch (EmptyStackException e) {
            didThrow = true;
        }
        assertTrue(didThrow);
    }
}