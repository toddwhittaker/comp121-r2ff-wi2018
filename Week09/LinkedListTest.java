package Week09;

import java.util.List;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class LinkedListTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class LinkedListTest extends AbstractListTest {
    /**
     * Default constructor for test class LinkedListTest.
     */
    public LinkedListTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        super.setUp();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        super.tearDown();
    }
    
    @Override
    protected <T> List<T> getListToTest(Class<T> clazz) {
        return new LinkedList<T>();
    }
}
