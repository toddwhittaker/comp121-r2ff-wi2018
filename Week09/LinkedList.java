package Week09;

/*#
 * NOTE WELL!!!! None of the test cases that involve removal of elements
 * will pass until the Week 8 homework assignment is completed!!
 */

import java.util.ListIterator;
import java.util.List;
import java.util.Collection;

/**
 * A concrete implementation of List that is based on links.
 * Since we inherit from AbstractList, there are only a few methods
 * to implement such as <tt>size</tt>, <tt>listIterator</tt>, and
 * <tt>subList</tt>.  But, it also defines methods unique to LinkedList
 * such as <tt>addFirst</tt> and <tt>addLast</tt>.
 * 
 * @param <E> the type of data to hold in the collection
 * 
 * @author  Franklin
 * @version 2.0.0.0
 */
public class LinkedList<E> extends AbstractList<E> {
    private int modCount;
    private Link<E> head;
    private int size;

    /**
     * Constructs an empty list.
     */
    public LinkedList() {
        this.modCount = 0;
        this.size = 0;
        this.head = new Link<E>(null, null, null);
        this.head.next = this.head;
        this.head.previous = this.head;
    }

    /**
     * Returns the number of elements in the list.
     * @return the number of elements in the list.
     */
    public int size() {
        return this.size;
    }

    /**
     * Returns a ListIterator over the elements in this list.  The starting
     * position for the list iterator will be the 0th element.
     * @return a ListIterator over the elements in this list.
     */
    public ListIterator<E> listIterator(int index) {
        return new LinkedListIterator(index);
    }

    /**
     * Inserts the specified element at the beginning of this list.
     * @param element - the element to add
     */
    public void addFirst(E element) {
        /*# TODO: this is a HW 8 problem. :( */
    }

    /**
     * Inserts the specified element at the end of this list.
     * @param element - the element to add
     */
    public void addLast(E element) {
        listIterator(size()).add(element);
    }

    /**
     * Returns the first element in the list.
     * @throws NoSuchElementException - if the list is empty
     */
    public E getFirst() {
        return listIterator(0).next();
    }

    /**
     * Returns the last element in the list.
     * @throws NoSuchElementException - if the list is empty
     */
    public E getLast() {
        return listIterator(size()).previous();
    }

    /**
     * Removes and returns the first element of the list.
     * @return the first element of the list
     * @throws java.util.NoSuchElementException if the list is empty.
     */
    public E removeFirst() {
        ListIterator<E> itr = listIterator(0);
        E result = itr.next();
        itr.remove();
        return result;
    }

    /**
     * Removes and returns the last element of the list.
     * @return the last element of the list
     * @throws java.util.NoSuchElementException if the list is empty.
     */
    public E removeLast() {
        ListIterator<E> itr = listIterator(size());
        E result = itr.previous();
        itr.remove();
        return result;
    }

    private static final class Link<E> {
        private E data;
        private Link<E> next;
        private Link<E> previous;

        public Link(Link<E> previous, E data, Link<E> next) {
            this.previous = previous;
            this.data = data;
            this.next = next;
        }

        public Link<E> prepend(E data) {
            Link<E> link = new Link<E>(this.previous, data, this);
            this.previous.next = link;
            this.previous = link;
            return link;
        }

        public Link<E> append(E data) {
            Link<E> link = new Link<E>(this, data, this.next);
            this.next.previous = link;
            this.next = link;
            return link;
        }

        public void remove() {
            this.previous.next = this.next;
            this.next.previous = this.previous;
            this.data = null;
        }

    }

    /**
     * An object that abstracts the idea of iterator (both forward and
     * backward) over a list of objects.  It also allows the replacement
     * of elements on the fly, and the extraction of the iterator position
     * in terms of an index value between elements (<tt>0</tt> being
     * <em>before</em>the 0th element, and <tt>n+1</tt> being <em>after</em>
     * the nth element).
     * @param <E> the type of data returned
     * 
     * @author Todd A. Whittaker
     * @version 2005-09
     */
    private class LinkedListIterator implements ListIterator<E> {
        private int expectedModCount;
        private Link<E> cursor;
        private Link<E> priorCursor;
        private int cursorIndex;

        /**
         * Construct a list iterator at the right cursor position.
         */
        public LinkedListIterator(int startIndex) {
            this.expectedModCount = LinkedList.this.modCount;
            if (startIndex < 0 || startIndex > size()) {
                throw new IndexOutOfBoundsException(startIndex +
                    " is out of bounds [0..." + size() + "]");
            }
            if (startIndex < size() / 2) {
                // first half -- fast forward from head
                this.cursor = head.next;
                for (int i = 0; i < startIndex; ++i) {
                    this.cursor = this.cursor.next;
                }
            } else {
                // second half -- rewind from the tail
                this.cursor = head;
                for (int i = size(); i > startIndex; --i) {
                    this.cursor = this.cursor.previous;
                }
            }
            this.cursorIndex = startIndex;
        }

        /**
         * Ensures that the expected and actual modification counts between the
         * list and the iterator are in sync with one another.
         * 
         * @throws java.util.ConcurrentModificationException when they aren't
         */
        private void checkForComodification() {
            if (this.expectedModCount != LinkedList.this.modCount) {
                throw new java.util.ConcurrentModificationException();
            }
        }

        /**
         * Re-synchronizes the modification counts between the list and the
         * iterator.
         */
        private void synchronizeModCounts() {
            this.expectedModCount = LinkedList.this.modCount;
        }

        /**
         * Updates and then re-synchronizes the modification counts
         * between the list and the iterator.
         */
        protected void updateAndSynchronizeModCounts() {
            ++LinkedList.this.modCount;
            this.synchronizeModCounts();
        }

        /**
         * Ensures that there was a prior call to either next or previous before
         * performing an operation on the last returned item.
         * 
         * @throws IllegalStateException when there was no prior call
         */
        private void checkForValid() {
            if (priorCursor == null) {
                throw new IllegalStateException("no prior call to next or previous");
            }
        }

        /**
         * Returns true when the iterator has more data to return.
         * @return true if the iterator has more data
         */
        public boolean hasNext() {
            return cursor != head;
        }

        /**
         * Returns the next element from the iteration.
         * @return the next data element
         */
        public E next() {
            checkForComodification();
            if (!hasNext()) {
                throw new java.util.NoSuchElementException("no next");
            }
            E result = cursor.data;
            priorCursor = cursor;
            cursor = cursor.next;
            ++cursorIndex;
            return result;
        }

        /**
         * Removes the previously returned element from the iteration.
         * @throws IllegalStateException when next is not called
         * immediately before remove.
         */
        public void remove() {
            // make sure that another iterator hasn't modified
            checkForComodification();
            // make sure there was a prior call to next or previous
            checkForValid();

            /*# TODO: this is a HW 8 problem. :( */

            // update and recync with the list
            updateAndSynchronizeModCounts();
        }

        /**
         * Inserts the specified element into the list at the present iterator
         * position.  The insertion point is before the element that would be
         * returned by a call to <tt>next</tt> and after the element that would
         * be returned by a call to <tt>previous</tt>.  After adding the element,
         * a call to <tt>previous</tt> will return the newly inserted element,
         * but a call to <tt>next</tt> will return the element immediately
         * after the one inserted.
         * @param obj the element to insert
         */
        public void add(E obj) {
            checkForComodification();
            Link<E> link = cursor.prepend(obj);
            ++size;
            ++cursorIndex;
            updateAndSynchronizeModCounts();
        }

        /**
         * Returns true when the iterator has more data to return in the
         * reverse direction.
         * @return true if the iterator has more data
         */
        public boolean hasPrevious() {
            return cursor.previous != head;
        }

        /**
         * Returns the element from the iteration when traversing in the
         * reverse direction.
         * @return the previous data element
         */
        public E previous() {
            checkForComodification();
            if (!hasPrevious()) {
                throw new java.util.NoSuchElementException("no previous");
            }
            cursor = cursor.previous;
            priorCursor = cursor;
            --cursorIndex;
            return cursor.data;
        }

        /**
         * Returns the index of the element that would be returned should
         * a subsequent call to <tt>next</tt> be made.
         * @return the index of the next element
         */
        public int nextIndex() {
            return cursorIndex;
        }

        /**
         * Returns the index of the element that would be returned should
         * a subsequent call to <tt>previous</tt> be made.
         * @return the index of the previous element
         */
        public int previousIndex() {
            return cursorIndex - 1;
        }

        /**
         * Replaces the last element returned by either <tt>next</tt> or
         * <tt>previous</tt>.  Cannot be called immediately after <tt>add</tt>
         * or <tt>remove</tt>.
         * @param obj the object with which to replace the one returned
         * earlier.
         */
        public void set(E obj) {
            /*# TODO: this is a HW 8 problem. :( */
        }
    }
}
