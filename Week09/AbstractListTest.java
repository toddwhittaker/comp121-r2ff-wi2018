package Week09;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.Iterator;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class AbstractListTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public abstract class AbstractListTest extends AbstractCollectionTest {

    private List<Integer> list;

    /**
     * Default constructor for test class AbstractListTest.
     */
    public AbstractListTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        super.setUp();
        list = getListToTest(Integer.class);
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        super.tearDown();
    }

    /**
     * Let the superclass get a collection to test from us.
     */
    @Override
    protected <T> Collection<T> getCollectionToTest(Class<T> clazz) {
        return getListToTest(clazz);
    }

    protected abstract <T> List<T> getListToTest(Class<T> clazz);

    @Test
    public void testGrowingLarge() {
        list = new ArrayList<Integer>(10);
        for (int i = 0; i < 100; ++i) {
            assertTrue(list.add(i));
        }
        assertEquals(100, list.size());
    }

    @Test
    public void testAddAtIterator() {
        int targetLocation = 2;
        fill(list, data);
        ListIterator<Integer> itr = list.listIterator(targetLocation);
        itr.add(99);
        assertEquals(data.length + 1, list.size());
        int index = 0;
        Iterator<Integer> itr2 = list.iterator();
        while (itr2.hasNext()) {
            Integer num = itr2.next();
            if (index == targetLocation) {
                assertEquals(99, num.intValue());
            }
            ++index;
        }
    }

    @Test
    public void testAddAtIndex() {
        fill(list, data);
        list.add(2, 99);
        assertTrue(list.toString().startsWith("[3, 1, 99, 4,"));
        assertEquals(data.length + 1, list.size());
    }

    @Test
    public void testNextIndex() {
        fill(list, data);
        ListIterator<Integer> itr = list.listIterator(3);
        assertEquals(3, itr.nextIndex());
    }

    @Test
    public void testIteratingBackwards() {
        fill(list, data);
        ListIterator<Integer> itr = list.listIterator(list.size());
        int index = data.length - 1;
        while (itr.hasPrevious()) {
            assertEquals(index, itr.previousIndex());
            Integer num = itr.previous();
            assertEquals(data[index], num.intValue());
            --index;
        }
        assertEquals(-1, index);
    }

    @Test
    public void testRemovingBackwards() {
        fill(list, data);
        ListIterator<Integer> itr = list.listIterator(list.size());
        while (itr.hasPrevious()) {
            itr.previous();
            itr.remove();
        }
        assertEquals(0, list.size());
    }

    @Test
    public void testSetIterator() {
        fill(list, data);
        ListIterator<Integer> itr = list.listIterator();
        while (itr.hasNext()) {
            Integer num = itr.next();
            itr.set(num + 1);
        }
        for (int i = 0; i < data.length; ++i) {
            assertEquals(data[i] + 1, list.get(i).intValue());
        }
    }

    @Test
    public void testSetAtIndex() {
        fill(list, data);
        for (int i = 0; i < list.size(); ++i) {
            list.set(i, list.get(i) - 1);
        }
        for (int i = 0; i < data.length; ++i) {
            assertEquals(data[i] - 1, list.get(i).intValue());
        }
    }

    @Test(expected = java.util.ConcurrentModificationException.class)
    public void testConcurrentModification() {
        fill(list, data);
        ListIterator<Integer> itr1 = list.listIterator(1);
        ListIterator<Integer> itr2 = list.listIterator(list.size());
        itr1.next();
        itr1.remove();
        itr2.previous(); // should throw
    }

    @Test
    public void testAddAllAtIndex() {
        fill(list, data);
        List<Integer> other = getListToTest(Integer.class);
        int [] otherData = {99, 98, 97};
        fill(other, otherData);
        list.addAll(2, other);
        assertEquals(data.length + otherData.length, list.size());
        assertEquals(99, list.get(2).intValue());
        assertEquals(98, list.get(3).intValue());
        assertEquals(97, list.get(4).intValue());
        assertEquals(3, list.get(0).intValue());
        assertEquals(1, list.get(1).intValue());
        assertEquals(4, list.get(5).intValue());

    }
}
