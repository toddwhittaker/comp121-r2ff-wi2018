package Week09;

import java.util.Collection;
import java.util.Iterator;

/**
 * Write a description of class UnmodifiableCollection here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class UnmodifiableCollection<E> implements Collection<E> {

    private Collection<E> collection;
    
    public UnmodifiableCollection(Collection<E> collection) {
        this.collection = collection;
    }
    
    public boolean add(E element) {
        return false;
    }
    
    public int size() {
        return collection.size();
    }
    
    public Iterator<E> iterator() {
        return new UnmodifiableIterator(collection.iterator());
    }
    public boolean contains(Object obj) {
        return collection.contains(obj);
    }

    public boolean isEmpty() {
        return collection.isEmpty(); 
    }

    public boolean remove(Object obj) {
        return false;
    }

    public void clear() {
    }

    public boolean addAll(Collection<? extends E> coll) {
        return false;
    }

    public boolean removeAll(Collection<?> coll) {
        return false;
    }

    public Object [] toArray() {
        return collection.toArray();
    }

    public <T> T[] toArray(T[] a) {
        return collection.toArray(a);
    }

    public boolean containsAll(Collection<?> coll) {
        return collection.containsAll(coll);
    }

    public boolean retainAll(Collection<?> coll) {
        return false;
    }
    
    private static class UnmodifiableIterator<E> implements Iterator<E> {
        private Iterator<E> base;
        
        public UnmodifiableIterator(Iterator<E> base) {
            this.base = base;
        }
        
        public boolean hasNext() {
            return base.hasNext();
        }
        
        public E next() {
            return base.next();
        }
        
        public void remove() {
            // do nothing;
        }
    }
}
