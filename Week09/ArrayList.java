package Week09;

import java.util.List;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

/**
 * List is an extension of collection that adds the concepts of order,
 * indexed operations and bi-directional iteration.  The order of the
 * collection is guaranteed (unlike sets or collections).  Operations
 * may now take place at particular indices.  Finally, the list iterator
 * can move both forward and backward through the list and add elements
 * as well as remove them at iterator positions.
 * @param <E> the type of data to hold in the list
 * 
 * @author Todd A. Whittaker
 * @version 20171018
 */
public class ArrayList<E> extends AbstractList<E> {

    /** The backing store for our list. */
    private E [] data;

    /** How many of those data are in use? */
    private int size;

    /** How many times has the list been modified? */
    private int modCount;

    /** The default capacity if none is specified. */
    private static final int DEFAULT_CAPACITY = 10;

    /**
     * Build an array list with a default size.
     */
    public ArrayList() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Build an array list with a given initial capacity.
     * @param initialCapacity
     */
    public ArrayList(int initialCapacity) {
        init(initialCapacity);
    }

    /**
     * Initialize the list as a pseudo-constructor
     */
    private void init(int initialCapacity) {
        this.data = Utilities.newArray(data, initialCapacity);
        this.size = 0;
        this.modCount = 0;
    }

    /**
     * Returns the number of elements in the collection.
     * @return the number of elements in the collection.
     */
    public int size() {
        return size;
    }

    /**
     * Make the data array hold more.
     */
    private void ensureCapacity(int requiredCapacity) {
        if (requiredCapacity > data.length) {
            int newCapacity = data.length * 2 + 1;
            if (requiredCapacity > newCapacity) {
                newCapacity = requiredCapacity;
            }
            E [] temp = Utilities.newArray(this.data, newCapacity);
            for (int i = 0; i < size; ++i) {
                temp[i] = data[i];
            }
            data = temp;
        }
    }


    @Override // for efficiency
    public boolean addAll(int index, Collection<? extends E> coll) {
        int delta = coll.size();
        ensureCapacity(size + delta);
        for (int i = size - 1; i >= index; --i) {
            data[i + delta] = data[i];
        }
        Iterator<? extends E> itr = coll.iterator();
        for (int i = index; i < index + delta; ++i) {
            data[i] = itr.next();
        }
        size += delta;
        return delta > 0;
    }

    @Override // for efficiency
    public void clear() {
        init(DEFAULT_CAPACITY);
    }

    @Override // for efficiency
    public boolean removeAll(Collection<?> coll) {
        E tombstone = (E)(new Object());
        int origSize = size();
        for (int i = 0; i < size; ++i) {
            if (coll.contains(data[i])) {
                data[i] = tombstone;
            }
        }
        removeTombstones(tombstone);
        return origSize != size();
    }

    @Override // for efficiency
    public boolean retainAll(Collection<?> coll) {
        E tombstone = (E)(new Object());
        int origSize = size();
        for (int i = 0; i < size; ++i) {
            if (!coll.contains(data[i])) {
                data[i] = tombstone;
            }
        }
        removeTombstones(tombstone);
        return origSize != size();
    }

    private void removeTombstones(E tombstone) {
        int delta = 0;
        int i = 0;
        while (i + delta  < size) {
            if (data[i + delta] == tombstone) {
                ++delta;
            } else {
                data[i] = data[i + delta];
                ++i;
            }
        }
        size -= delta;
    }

    /**
     * Returns a ListIterator over the elements in this list starting at
     * the given position.
     * @param index the starting index for iteration
     * @throws IndexOutOfBoundsException if the index < 0 or index > size()
     */
    public ListIterator<E> listIterator(int index) {
        return new ArrayListIterator(index);
    }

    /**
     * An object that abstracts the idea of iterator (both forward and
     * backward) over a list of objects.  It also allows the replacement
     * of elements on the fly, and the extraction of the iterator position
     * in terms of an index value between elements (<tt>0</tt> being
     * <em>before</em>the 0th element, and <tt>n+1</tt> being <em>after</em>
     * the nth element).
     * @param <E> the type of data returned
     * 
     * @author Todd A. Whittaker
     * @version 2005-09
     */
    private class ArrayListIterator implements ListIterator<E> {

        private int cursor;
        private int priorCursor; // index of the last thing returned by next() or previous()
        private static final int BAD_CURSOR = -1;
        private int expectedModCount;

        public ArrayListIterator(int startIndex) {
            if (startIndex < 0 || startIndex > size()) {
                throw new IndexOutOfBoundsException();
            }
            cursor = startIndex;
            priorCursor = BAD_CURSOR;
            expectedModCount = modCount;
        }

        private void checkForComodification() {
            if (expectedModCount != modCount) {
                throw new java.util.ConcurrentModificationException();
            }
        }

        /**
         * Returns true when the iterator has more data to return.
         * @return true if the iterator has more data
         */
        public boolean hasNext() {
            return cursor < size();
        }

        /**
         * Returns the next element from the iteration.
         * @return the next data element
         */
        public E next() {
            checkForComodification();
            if (!hasNext()) {
                throw new java.util.NoSuchElementException();
            }
            priorCursor = cursor;
            return data[cursor++];
        }

        /**
         * Removes the previously returned element from the iteration.
         * @throws IllegalStateException when next is not called
         * immediately before remove.
         */
        public void remove() {
            checkForComodification();
            if (priorCursor == BAD_CURSOR) {
                throw new IllegalStateException();
            }
            for (int i = priorCursor; i < size() - 1; ++i) {
                data[i] = data[i+1];
            }
            cursor = priorCursor;
            data[--size] = null;
            priorCursor = BAD_CURSOR;
            updateAndSynchronizeModCounts();
        }

        private void updateAndSynchronizeModCounts() {
            expectedModCount = ++modCount;
        }

        /**
         * Inserts the specified element into the list at the present iterator
         * position.  The insertion point is before the element that would be
         * returned by a call to <tt>next</tt> and after the element that would
         * be returned by a call to <tt>previous</tt>.  After adding the element,
         * a call to <tt>previous</tt> will return the newly inserted element,
         * but a call to <tt>next</tt> will return the element immediately
         * after the one inserted.
         * @param obj the element to insert
         * @throws ConcurrentModificationException
         */
        public void add(E obj) {
            checkForComodification();
            ensureCapacity(size + 1);
            for (int i = size(); i > cursor; --i) {
                data[i] = data[i - 1];
            }
            data[cursor++] = obj;
            ++size;
            updateAndSynchronizeModCounts();
        }

        /**
         * Returns true when the iterator has more data to return in the
         * reverse direction.
         * @return true if the iterator has more data
         */
        public boolean hasPrevious() {
            return cursor > 0;
        }

        /**
         * Returns the index of the element that would be returned should
         * a subsequent call to <tt>next</tt> be made.
         * @return the index of the next element
         */
        public int nextIndex() {
            return cursor;
        }

        /**
         * Returns the element from the iteration when traversing in the
         * reverse direction.
         * @return the previous data element
         */
        public E previous() {
            checkForComodification();
            if (!hasPrevious()) {
                throw new java.util.NoSuchElementException();
            }
            priorCursor = --cursor;
            return data[cursor];
        }

        /**
         * Returns the index of the element that would be returned should
         * a subsequent call to <tt>previous</tt> be made.
         * @return the index of the previous element
         */
        public int previousIndex() {
            return cursor - 1;
        }

        /**
         * Replaces the last element returned by either <tt>next</tt> or
         * <tt>previous</tt>.  Cannot be called immediately after <tt>add</tt>
         * or <tt>remove</tt>.
         * @param obj the object with which to replace the one returned
         * earlier.
         */
        public void set(E obj) {
            /*# This is a HW 7 problem. :( */
            checkForComodification();
            //checkForValid();

            // whatever we handed back last is what we overwrite
            data[priorCursor] = obj;

        }

    }
}
