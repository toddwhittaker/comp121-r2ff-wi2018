package Week09;

import java.util.*;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class UnmodifiableCollectionTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class UnmodifiableCollectionTest {
    
    UnmodifiableCollection<Integer> collection;
    
    /**
     * Default constructor for test class UnmodifiableCollectionTest.
     */
    public UnmodifiableCollectionTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.setUp();
        Collection<Integer> base = new ArrayList<Integer>();
        for (int i = 0; i < 10; ++i) {
            base.add(i*2);
        }
        collection = new UnmodifiableCollection<Integer>(base);
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.tearDown();
    }
    

    @Test
    public void testSize() {
        assertEquals(10, collection.size());
    }
    
    @Test
    public void testAdd() {
        assertFalse(collection.add(99));
        assertFalse(collection.contains(99));
        assertEquals(10, collection.size());
    }
    
    @Test
    public void testIteratorRemove() {
        Iterator<Integer> itr = collection.iterator();
        itr.next();
        itr.remove();
        assertEquals(10, collection.size());
    }
}
