package Week09;

import java.util.Collection;
import java.util.Iterator;

/**
 * Write a description of class AbstractCollection here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public abstract class AbstractCollection<E> implements Collection<E> {

    /**
     * Returns true if the collection contains no elements.
     * @return true if the collection is empty.
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Searches the collection to determine if the collection contains
     * an element that matches the specified object.  If the specified
     * object is null, it will look for a null in the collection.  Otherwise
     * it will use the <tt>equals</tt> method of the given object to 
     * determine equality.  That is, <tt>o == null ? element == null : 
     * o.equals(element)</tt>.
     * @param obj the object to match
     * @return true if the collection contains a matching element
     */
    public boolean contains(Object obj) {
        Iterator<E> itr = this.iterator();
        while (itr.hasNext()) {
            if (Utilities.nullSafeEquals(obj, itr.next())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Removes the first element of this collection matching the parameter
     * object <tt>obj</tt>.  If the colleciton is altered as a result of
     * the operation, <tt>remove</tt> returns <tt>true</tt>.
     * @param obj the object to match
     * @return true if the collection is altered, false otherwise.
     */
    public boolean remove(Object obj) { // O(n) for linked, O(n) for array
        Iterator<E> itr = this.iterator();
        while (itr.hasNext()) {
            if (Utilities.nullSafeEquals(obj, itr.next())) {
                itr.remove(); // O(1) for linked, O(n) for array
                return true;
            }
        }
        return false;
    }

    /**
     * Removes all elements from the collection.  The collection will
     * be empty after calling this method.
     */
    @Override
    public void clear() { // O(n) for linked, O(n^2) for array.
        Iterator<E> itr = this.iterator();
        while (itr.hasNext()) {
            itr.next();
            itr.remove(); // O(1) for linked, O(n) for array
        }
    }

    /**
     * Inserts all the elements of <tt>coll</tt> into this collection.  If
     * both this collection and the parameter are the same collection, then
     * the operational behavior is undefined (i.e. bad things can happen).
     * @param coll the collection from which to draw elements for addition.
     * @return true when this collection is modified as a result.
     */
    public boolean addAll(Collection<? extends E> coll) { // O(m)
        int originalSize = size();
        for (E element : coll) {
            this.add(element);
        }
        return originalSize != size();
    }

    /**
     * Removes all elements in this collection that match those in the
     * parameter collection <tt>coll</tt>.  When the operation completes,
     * the two collections will be disjoint.
     * @param coll the collection of elements to be removed from this one.
     * @return true if this collection is altered as a result of the call.
     */
    public boolean removeAll(Collection<?> coll) { // O(m*n^2) for both
        int originalSize = size();
        for (Object obj : coll) {
            while (this.remove(obj)) {
                // empty;
            }
        }
        return originalSize != size();
    }

    /**
     * Compares the parameter object <tt>obj</tt> against this collection
     * for equality.  Care should be taken to ensure symmetry, transitivity,
     * and reflexivity properties of equality.  That is: if
     * <tt>a.equals(b)</tt> is true then <tt>b.equals(a)</tt> should also be
     * true (symmetry); if <tt>a.equals(b)</tt> and <tt>b.equals(c)</tt> then
     * <tt>a.equals(c)</tt> should be true (transitivity); and also
     * <tt>a.equals(a)</tt> should also be true.
     * 
     * The contract between <tt>equals</tt> and <tt>hashCode</tt> should
     * also be kept.  That is, if <tt>a.equals(b)</tt> is true then
     * <tt>a.hashCode() == b.hashCode()</tt> should also be true.  The
     * reverse is not true (equal hash codes do not imply object equality).
     * 
     * A typical way to implement <tt>equals</tt> would be do do a value
     * comparison of the contents of the collection.
     * 
     * @param obj the object against which to compare this collection.
     * @return true if the object is equal to this collection.
     */
    public boolean equals(Object obj) {
        if (!(obj instanceof Collection)) {
            return false;
        }
        Collection<Object> other = (Collection<Object>)obj;
        if (other.size() != this.size()) {
            return false;
        }
        // same order and same data
        Iterator<Object> oItr = other.iterator();
        Iterator<E> eItr = iterator();
        while (oItr.hasNext() && eItr.hasNext()) {
            Object o = oItr.next();
            E e = eItr.next();
            if (!Utilities.nullSafeEquals(o, e)) {
                return false;
            }
        }
        return true;    
    }

    /**
     * Returns the hash code value for this collection.  A typical
     * implementation would be to somehow aggregate the hash codes of the
     * individual elements of the collection.  Be sure that hashCode is
     * also overridden if equals is overridden.
     * @return the hash code of the collection.
     */
    public int hashCode() {
        final int MULTIPLIER = 31;
        int result = 0;
        for (E element : this) {
            result = result * MULTIPLIER;
            if (element != null) {
                result += element.hashCode();
            }
        }
        return result;
    }

    /**
     * Creates and returns an array containing the data from this collection.
     * If this collection preserves order, then the order in the array
     * will be the same as the order of the collection.  The array returned
     * will always be safe to be modified (i.e. it will be different from
     * the underlying collection's array if the collection is array-backed).
     * @return an array containing references to the collections elements.
     */
    public Object [] toArray() {
        int i = 0;
        Object [] result = new Object[this.size()];
        for (E element : this) {
            result[i++] = element;
        }
        return result;
    }

    /**
     * Returns an array containing the data from this collection.  If the
     * parameter array is large enough to hold the collection, then it is
     * filled and returned.  Otherwise, a new array of the same type is
     * allocated, filled, and returned.  If this collection preserves order
     * then the order in the array will be the same as the order of the
     * collection. The array returned will always be safe to be modified
     * (i.e. it will be different from the underlying collection's array
     * if the collection is array-backed). The last element of the array
     * will be set to null if there is sufficient room.
     * 
     * @param a - the array to fill with the elements of this collection. If
     * the array is large enough, then the elements [0, size()-1] will be
     * overwritten. Otherwise, a new array will be constructed.
     * @return an array containing references to the collections elements.
     */
    public <T> T[] toArray(T[] a) {
        T [] result = a;
        int i = 0;
        if (a == null || a.length < size()) {
            result = Utilities.newArray(a, size());
        }
        for (E element : this) {
            result[i++] = (T)element;
        }
        while (i < a.length) {
            a[i++] = null;
        }
        return result;
    }

    /**
     * Searches this collection to determine if every element of <tt>coll</tt>
     * exists in this collection.  Comparison is based on the same algorithm
     * as for <tt>contains</tt>.
     * @param coll the collection to be checked for containment in this on
     * @return true if all elements of <tt>coll</tt> also exist in this.
     */
    public boolean containsAll(Collection<?> coll) { // O(n*m)
        /*# TODO: this is a Homework problem :( */
        for (Object obj : coll) {
            if (!this.contains(obj)) {
                return false;
            }
        }
        return true;        
    }

    /**
     * Retains all the elements in this collection that match those in
     * the parameter collection <tt>coll</tt>.  That is, it will remove
     * all elements in this collection that have no match in <tt>coll</tt>.
     * @parma coll the collection of elements to be matched against
     * @return true if this collection is altered as a result of the call.
     */
    public boolean retainAll(Collection<?> coll) { //O(n*(n+m))
        /*# TODO: this is a Homework problem :( */
        boolean modified = false;
        for (Iterator<E> itr = this.iterator(); itr.hasNext(); ) {
            E element = itr.next();
            if (!coll.contains(element)) {
                itr.remove();
                modified = true;
            }
        }
        return modified;
    }

    /**
     * Build a nice string representation.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder("[");
        Iterator<E> itr = iterator();
        for (int i = 0; i < size(); ++i) {
            E element = itr.next();
            builder.append(element);
            if (i < size() - 1) {
                builder.append(", ");
            }
        }
        builder.append("]");
        return builder.toString();
    }

}
