package Week04;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import java.util.Comparator;

import java.io.File;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.Serializable;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;

/**
 * Write a description of class Company here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Company implements Serializable {
    private Employee [] employees;
    private int size;
    private int nextId;
    private static final long serialVersionUID = 1L;

    public Company() {
        init();
        nextId = 0;
    }

    private void init() {
        employees = new Employee[100];
        size = 0;
    }

    public int getHeadCount() {
        return size;
    }

    public boolean hire(Employee e) {
        if (size < employees.length) {
            employees[size++] = e;
            e.setHireId(nextId++);
            return true;
        }
        return false;
    }

    private int findEmployeeById(int id) {
        for (int i = 0; i < size; ++i) {
            if (employees[i].getHireId() == id) {
                return i;
            }
        }
        return -1;
    }

    private int indexOfEmployee(Employee emp) {
        for (int i = 0; i < size; ++i) {
            if (employees[i].equals(emp)) {
                return i;
            }
        }
        return -1;
    }

    public boolean fire(int id) {
        if (id < 0) {
            return false;
        }
        int index = findEmployeeById(id);
        if (index >= 0) {
            employees[index].setHireId(-1);
            employees[index] = employees[size-1];
            employees[size-1] = null;
            --size;
            return true;
        }
        return false;
    }

    public void giveRaiseToAll(double percent) {
        for (int i = 0; i < size; ++i) {
            employees[i].getRaise(percent);
        }
    }

    public void editEmployee(int id, Console console) {
        int index = findEmployeeById(id);
        if (index >= 0) {
            employees[index].edit(console);
        }
    }

    public String payAll() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size; ++i) {
            builder.append(String.format("Pay %s %s\n",
                    employees[i].getName(), employees[i].calculateGrossPay()));
        }
        return builder.toString();
    }

    public void sortEmployees(Comparator<Employee> cmp) {
        // selection sort
        for (int i = 0; i < size; ++i) {
            int minIndex = i;
            for (int j = i+1; j < size; ++j) {
                if (cmp.compare(employees[j], employees[minIndex]) < 0) {
                    minIndex = j;
                }
            }
            Employee temp = employees[i];
            employees[i] = employees[minIndex];
            employees[minIndex] = temp;
        }
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size; ++i) {
            builder.append(String.format("%s\n", employees[i].toString()));
        }
        return builder.toString();
    }

    public String[][] processFields(String line) {
        String [] fields = line.split(",");
        String [][] result = new String[fields.length][];
        for (int i = 0; i < fields.length; ++i) {
            result[i] = fields[i].split(":");
            for (int j = 0; j < result[i].length; ++j) {
                result[i][j] = result[i][j].trim();
            }
        }
        return result;
    }

    public void processLine(String line) {
        String type = line.substring(0, line.indexOf(" "));
        Employee employee = null;

        try {
            Class<?> cls = Class.forName("Week04." + type);
            employee = (Employee)cls.newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        String[][] fields = processFields(line.substring(
                    line.indexOf("{") + 1, line.indexOf("}")));
        employee.fillFields(fields);
        System.out.println(employee);
        hire(employee);

    }

    public int readFromTextFile(String fileName) {
        int result = 0;
        File f = new File(fileName);
        Scanner in = null;

        init();
        try {
            in = new Scanner(f);
            while (in.hasNextLine()) {
                String line = in.nextLine();
                ++result;
                processLine(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                in.close();
            }
        }
        return result;
    }

    public int writeToTextFile(String fileName) {
        int result = 0;

        File f = new File(fileName);
        PrintWriter out = null;

        try {
            out = new PrintWriter(f);
            for (int i = 0; i < size; ++i) {
                out.println(employees[i]);
                ++result;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
        return result;
    }

    public void readFromBinaryFile(String fileName) {
        init();
        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(new java.io.FileInputStream(fileName));
            Company temp = (Company)ois.readObject();
            this.employees = temp.employees;
            this.size = temp.size;
            this.nextId = temp.nextId;
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                }
            }
        }
    }
    
    public void writeToBinaryFile(String fileName) {
        ObjectOutputStream oos = null;

        try {
            oos = new ObjectOutputStream(new java.io.FileOutputStream(fileName));
            oos.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                }
            }
        }

    }

    public static Employee createRandomEmployee() {
        if (names == null || names.size() <= 0) {
            names = new ArrayList(java.util.Arrays.asList(rawNames));
        }

        if (r == null) {
            r = new Random(31337);
        }

        Money rate = Money.ZERO;
        double hours = 0;

        int type = r.nextInt(2) + 1;
        String name = names.get(r.nextInt(names.size()));

        names.remove(name);
        if (type == 1) {
            rate = new Money(r.nextDouble()*30 + 20);
            hours = r.nextInt(30) + 20;
            return new Hourly(name, rate, hours);
        }
        rate = new Money(r.nextDouble()*40000 + 40000);
        return new Salaried(name, rate);
    }

    // https://bitbucket.org/toddwhittaker/comp121-r2ff-wi2018
    private static Random r;  
    private static List<String> names;
    private static final String[] rawNames = new String [] {
            "Albus Dumbledore",
            "Minerva McGonnagal",
            "Severus Snape",
            "Filius Flitwick",
            "Horace Slughorn",
            "Argus Filch",
            "Gilderoy Lockhart",
            "Quirinus Quirrell",
            "Pomana Sprout",
            "Sybill Trelawney",
            "Rubeus Hagrid",
            "Charity Burbage",
            "Rolanda Hooch",
            "Remus Lupin",
            "Alastor Moody",
            "Poppy Pomfrey",
            "Dolores Umbridge"
        };

}
