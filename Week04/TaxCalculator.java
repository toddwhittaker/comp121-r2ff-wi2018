package Week04;


/**
 * Write a description of interface TaxCalculator here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public interface TaxCalculator {
    Money calculateTax(Money yearlyAmount);
}
