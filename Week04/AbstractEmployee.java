package Week04;

/**
 * Write a description of class Employee here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public abstract class AbstractEmployee implements Employee {
    private static final long serialVersionUID = 1L;
    protected static final int PAY_PERIODS = 52;
    private String name;
    private int id;

    public AbstractEmployee(String name) {
        setName(name);
        this.id = -1;
    }

    AbstractEmployee() {
    }

    public void setHireId(int id) {
        this.id = id;
    }

    public int getHireId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || name.trim().equals("")) {
            throw new BadNameException("Name cannot be null or empty.");
        }
        this.name = name.trim();
    }

    @Override
    public int compareTo(Employee other) {
        // compare by names
        String myLastName = this.getName().split(" ")[1];
        String otherLastName = other.getName().split(" ")[1];
        return myLastName.compareTo(otherLastName);
    }

    public Employee edit(Console console) {
        boolean done = false;
        while (!done) {
            try {
                setName(console.readLine("Name", this.name));
                done = true;
            } catch (IllegalArgumentException e) {
                console.println(e.getMessage());
            } finally {
                console.println("Finally!");
            }
        }
        return this;
    }

    public String toString() {
        return getClass().getSimpleName() + " {" + fields() + "}";
    }

    protected String fields() {
        return "name:" + this.name 
        + ", id:" + this.id;
    }

    @Override
    public boolean equals(Object obj) {
        // Object class does this:
        // return this == obj;

        if (obj == null || !(obj instanceof AbstractEmployee)) {
            return false;
        }
        AbstractEmployee emp = (AbstractEmployee)obj;
        return Utilities.nullSafeEquals(this.name, emp.name) &&
        this.id == emp.id;
    }

    @Override
    public final Money calculateNetPay(TaxCalculator calc) {
        // calculate gross pay and subtract taxes to get net pay.
        Money gross = calculateGrossPay();
        Money taxes = calc.calculateTax(
                gross.mul(PAY_PERIODS)).div(PAY_PERIODS);
        return gross.sub(taxes);
    }

    @Override
    public AbstractEmployee fillFields(String [][] fields) {
        for (String [] field : fields) {
            switch (field[0]) {
                case "name" : setName(field[1]); break;
                case "id" : setHireId(Integer.parseInt(field[1])); break;
            }
        }
        return this;
    }
}
