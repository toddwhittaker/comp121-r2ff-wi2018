package Week04;
import java.util.Comparator;

/**
 * Write a description of class Driver here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Driver {
    private Menu menu;
    private final Console console;
    private final Company company;

    private Driver() {
        console = new Console();
        company = new Company();
        for (int i = 0; i < 10; ++i) {
            company.hire(Company.createRandomEmployee());
        }
        menu = new Menu("Company menu", console);
        menu.addItem("List employees", () -> {
                console.println(company.toString());
            });
        menu.addItem("Hire employee", () -> {
                company.hire(makeEmployee());
            });
        menu.addItem("Fire employee", () -> {
                company.fire(console.readInt("Enter employee id", -1));
            });
        menu.addItem("Edit employee", () -> {
                company.editEmployee(console.readInt("Enter employee id", -1), console);
            });
        menu.addItem("Pay employees", () -> {
                console.println(company.payAll());
            });
        menu.addItem("Give raises", () -> {
                company.giveRaiseToAll(console.readDouble("What percentage", 0.05));
            });
        menu.addItem("Sort by last name", () -> {
                company.sortEmployees( (Employee e1, Employee e2) -> {
                        String myLastName = e1.getName().split(" ")[1];
                        String otherLastName = e2.getName().split(" ")[1];
                        return myLastName.compareTo(otherLastName);
                    });
            });
        menu.addItem("Sort by ID", () -> {
                company.sortEmployees( (Employee e1, Employee e2) -> {
                        return e1.getHireId() - e2.getHireId();
                    });
            });
        menu.addItem("Write to text file", () -> {
                String fileName = console.readLine("File name", "Week04\\company.txt");
                int count = company.writeToTextFile(fileName);
                console.printf("Wrote %d records\n", count);
            });
        menu.addItem("Read from text file", () -> {
                String fileName = console.readLine("File name", "Week04\\company.txt");
                int count = company.readFromTextFile(fileName);
                console.printf("Read %d records\n", count);
            });
        menu.addItem("Write to binary file", () -> {
                String fileName = console.readLine("File name", "Week04\\company.dat");
                company.writeToBinaryFile(fileName);
            });
        menu.addItem("Read from binary file", () -> {
                String fileName = console.readLine("File name", "Week04\\company.dat");
                company.readFromBinaryFile(fileName);
            });
        boolean done = false;
        while (!done) {
            try {
                menu.menuLoop();
                done = true;
            } catch (Exception e) {
                console.println("Oops. " + e.getMessage());
            }
        }
    }

    private Employee makeEmployee() {
        Employee e = null;
        int type = -1;
        while (type != 1 && type != 2) {
            type = console.readInt("Type (1=hourly, 2=salaried): ");
        }
        if (type == 1) {
            e = new Hourly("REPLACE ME", Money.ZERO, 0);
        } else {
            e = new Salaried("REPLACE ME", Money.ZERO);
        }
        e.edit(console);
        return e;
    }

    public static void main(String [] args) {
        Driver driver = new Driver();
    }
}
