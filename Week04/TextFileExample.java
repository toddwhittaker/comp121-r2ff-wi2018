package Week04;
import java.util.*;
import java.io.*;

/**
 * Write a description of class TextFileExample here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class TextFileExample {
    public static void main(String [] args) {
        String fileName = "Week04\\myTextFile.txt";
        List<String> strings = new ArrayList<String>();
        strings.add("The first line of the file\r\n");
        for (int i = 0; i < 10; ++i) {
            strings.add(String.format("This is line %d\r\n", i+2));
        }
        
        writeToFile(fileName, strings);
        List<String> result = readFromFile(fileName);
        System.out.println(result);
    }
    public static List<String> readFromFile(String fileName) {
        List result = new ArrayList<String>();
        File f = new File(fileName);
        Scanner in = null;
        
        try {
            in = new Scanner(f);
            while (in.hasNextLine()) {
                String s = in.nextLine();
                result.add(s);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                in.close();
            }
        }
        
        return result;
    }
    
    public static void writeToFile(String fileName, List<String> strings) {
        File f = new File(fileName);
        PrintWriter out = null;
        
        try {
            out = new PrintWriter(f);
            for (String s : strings) {
                out.print(s);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
}
