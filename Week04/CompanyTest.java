package Week04;



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class CompanyTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class CompanyTest {
    private Company company;
    
    /**
     * Default constructor for test class CompanyTest.
     */
    public CompanyTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super();
        company = new Company();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.setUp();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.tearDown();
    }
    
    @Test
    public void testConstruction() {
        assertNotNull(company);
        assertEquals(0, company.getHeadCount());
    }
    
    @Test
    public void testHire1() {
        Employee e1 = Company.createRandomEmployee();
        Employee e2 = Company.createRandomEmployee();
        assertTrue(company.hire(e1));
        assertEquals(1, company.getHeadCount());
        assertTrue(company.hire(e2));
        assertEquals(2, company.getHeadCount());
    }

    @Test
    public void testHire2() {
        Employee e1 = Company.createRandomEmployee();
        assertTrue(company.hire(e1));
        assertFalse(company.hire(e1));
        assertEquals(1, company.getHeadCount());
    }
    
    @Test
    public void testFire1() {
        Employee e1 = Company.createRandomEmployee();
        Employee e2 = Company.createRandomEmployee();
        Employee e3 = Company.createRandomEmployee();
        company.hire(e1);
        company.hire(e2);
        company.hire(e3);
        assertTrue(company.fire(e1.getHireId()));
        assertFalse(company.fire(e1.getHireId()));
        assertEquals(2, company.getHeadCount());
    }
    
    @Test
    public void testToString() {
        Employee e1 = Company.createRandomEmployee();
        Employee e2 = Company.createRandomEmployee();
        Employee e3 = Company.createRandomEmployee();
        company.hire(e1);
        company.hire(e2);
        company.hire(e3);
        
        String s = company.toString();
        assertTrue(s.contains(e1.toString()));
        assertTrue(s.contains(e2.toString()));
        assertTrue(s.contains(e2.toString()));
    }
}
