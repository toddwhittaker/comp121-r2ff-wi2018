package Week04;
import java.io.Serializable;

/**
 * Write a description of interface Employee here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public interface Employee extends Person, Comparable<Employee>, Serializable {
    Money calculateGrossPay();
    Money calculateNetPay(TaxCalculator calc);
    void setHireId(int id);
    int getHireId();
    Employee getRaise(double percent);
    Employee edit(Console console);
    Employee fillFields(String [][] fields);
}
