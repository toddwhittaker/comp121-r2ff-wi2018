package Week04;


/**
 * Write a description of class Menu here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Menu {
    private MenuItem [] items;
    private int itemCount;
    private String title;
    private int maxItemLength;
    private Console console;
    
    /**
     * Constructor for objects of class Menu.
     */
    public Menu(String title, Console console) {
        items = new MenuItem[20];
        itemCount = 0;
        this.title = title.trim();
        maxItemLength = this.title.length();
        this.console = console;
    }
    
    public void setTitle(String title) {
        this.title = title.trim();
        if (maxItemLength < this.title.length()) {
            maxItemLength = this.title.length();
        }
    }
    
    public void addItem(String itemName, MenuAction action) {
        itemName = itemName.trim();
        items[itemCount++] = new MenuItem(itemName, action);
        if (maxItemLength < itemName.length()) {
            maxItemLength = itemName.length();
        }
    }
    
    public String toString() {
        StringBuilder builder = new StringBuilder();
        String itemFormat = "%2d. %s\n";
        builder.append(title + "\n");
        builder.append(repeat(maxItemLength+4, '=') + "\n");
        for (int i = 0; i < itemCount; ++i) {
            builder.append(String.format(itemFormat, i+1, items[i]));
        }
        builder.append(String.format(itemFormat, quitChoice(), "Quit\n"));
        return builder.toString();
    }
    
    private static String repeat(int n, char ch) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < n; ++i) {
            builder.append(ch);
        }
        return builder.toString();
    }
    
    public int quitChoice() {
        return itemCount + 1;
    }
    
    public void menuLoop() {
        int choice = 0;
        do {
            console.print(this.toString());
            choice = console.readInt(String.format("Enter choice (1-%d): ",
                this.quitChoice()));
            if (choice >= 1 && choice < quitChoice()) {
                items[choice-1].select();
            } else if (choice != quitChoice()) {
                console.println("Invalid choice.");
            }
        } while (choice != quitChoice());
        console.print("Bye!\n");
    }
    
    private class MenuItem {
        public String itemName;
        public MenuAction action;
        
        public MenuItem(String itemName, MenuAction action) {
            this.itemName = itemName;
            this.action = action;
        }
        
        public void select() {
            action.performAction();
        }
        
        public String toString() {
            return itemName;
        }
    }
}
