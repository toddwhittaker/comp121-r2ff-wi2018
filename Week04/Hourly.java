package Week04;

/**
 * Write a description of class Hourly here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Hourly extends AbstractEmployee {
    private Money payRate;
    private double hours;

    public Hourly(String name, Money payRate, double hours) {
        super(name);
        setPayRate(payRate);
        setHoursWorked(hours);
    }

    Hourly() {
    }
    
    public Hourly setPayRate(Money payRate) {
        if (payRate == null || payRate.compareTo(Money.ZERO) < 0) {
            throw new IllegalArgumentException("Pay rate must be >= 0");
        }
        this.payRate = payRate;
        return this;
    }

    public Money getPayRate() {
        return this.payRate;
    }

    @Override
    public Hourly getRaise(double percent) {
        this.setPayRate(this.getPayRate().mul(1+percent));
        return this;
    }

    public Hourly setHoursWorked(double hours) {
        if (hours < 0) {
            throw new IllegalArgumentException("Hours must be >= 0");
        }
        this.hours = hours;
        return this;
    }

    public double getHoursWorked() {
        return this.hours;
    }

    @Override
    public Money calculateGrossPay() {
        double worked = hours;
        if (hours > 40) {
            worked = 40 + (1.5 * (hours - 40));
        }
        return payRate.mul(worked);
    }

    @Override
    public Hourly edit(Console console) {
        super.edit(console);
        setPayRate(new Money(console.readDouble(
                    "Pay rate", payRate.toDouble())));
        setHoursWorked(console.readDouble("Hours worked", hours));
        return this;
    }

    @Override
    protected String fields() {
        return super.fields()
            + ", payRate:" + this.payRate
            + ", hours:" + String.format("%.1f", this.hours);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        
        /* OR 
        if (obj == null || !(obj instanceof Hourly)) {
            return false;
        }
        */
       
        Hourly other = (Hourly)obj;
        return super.equals(other) &&
            Utilities.nullSafeEquals(this.payRate, other.payRate) &&
            Math.abs(this.hours - other.hours) < 0.0001;

    }
    
    @Override
    public Hourly fillFields(String [][] fields) {
        super.fillFields(fields);
        for (String [] field : fields) {
            switch (field[0]) {
                case "hours" : setHoursWorked(Double.parseDouble(field[1])); break;
                case "payRate" : setPayRate(Money.fromString(field[1])); break;
            }
        }
        return this;
    }
}
