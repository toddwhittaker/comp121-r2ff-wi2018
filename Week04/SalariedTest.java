package Week04;



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class SalariedTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class SalariedTest {
    /**
     * Default constructor for test class SalariedTest.
     */
    public SalariedTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.setUp();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.tearDown();
    }
    
    @Test
    public void testConstruction() {
        Salaried e = new Salaried("Albus Dumbledore",
            new Money(52000.0));
        assertNotNull(e);
    }
    
    @Test
    public void testHourlyCalculatePay1() {
        Salaried e = new Salaried("Albus Dumbledore",
            new Money(52000.0));
        assertEquals(new Money(1000.0), e.calculateGrossPay());
    }
    
    @Test
    public void testEdit() {
        String input = "Fred Weasley\n104000\n";
        Salaried e = new Salaried("Albus Dumbledore",
            new Money(52000.0));
        Console console = new Console(input);
        e.edit(console);
        assertEquals("Fred Weasley", e.getName());
        assertEquals(new Money(104000.0), e.getSalary());
    }
    
    @Test
    public void testToString1() {
        Salaried e = new Salaried("Albus Dumbledore",
            new Money(52000.0));
        String s = e.toString();
        assertTrue(s.contains("name:Albus Dumbledore"));
        assertTrue(s.contains("salary:$52000.00"));
    }
}
