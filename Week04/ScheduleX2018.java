package Week04;


/**
 * Write a description of class ScheduleX2018 here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ScheduleX2018 extends AbstractTaxCalculator {
    /**
     * Constructor for objects of class ScheduleX2018.
     */
    public ScheduleX2018() {
        super();
        super.addSchedule(new TaxSchedule(
            Money.ZERO, new Money(9525.0), 0.10));
        super.addSchedule(new TaxSchedule(
            new Money(9525.0), new Money(38700.0), 0.12));
        super.addSchedule(new TaxSchedule(
            new Money(38700.0), new Money(82500.0), 0.22));
        super.addSchedule(new TaxSchedule(
            new Money(82500.0), new Money(157500.0), 0.24));
        super.addSchedule(new TaxSchedule(
            new Money(157500.0), new Money(200000.0), 0.32));
        super.addSchedule(new TaxSchedule(
            new Money(200000.0), new Money(500000.0), 0.35));
        super.addSchedule(new TaxSchedule(
            new Money(500000.0), Money.MAX, 0.37));
            
    }
}
