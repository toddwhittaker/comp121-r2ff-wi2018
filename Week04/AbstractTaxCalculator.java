package Week04;
import java.util.List;
import java.util.ArrayList;

/**
 * Write a description of class TaxCalculator here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public abstract class AbstractTaxCalculator implements TaxCalculator {
    List<TaxSchedule> taxSchedules;

    public AbstractTaxCalculator() {
        taxSchedules = new ArrayList<TaxSchedule>();
    }

    protected void addSchedule(TaxSchedule schedule) {
        taxSchedules.add(schedule);
    }

    public Money calculateTax(Money yearlyAmount) {
        Money result = Money.ZERO;
        for (TaxSchedule schedule : taxSchedules) {
            result = result.add(schedule.taxesOn(yearlyAmount));
        }
        return result;
    }

    public class TaxSchedule {
        public Money lowBound;
        public Money highBound;
        public double percentage;

        public TaxSchedule(Money lowBound, Money highBound, double percentage) {
            this.lowBound = lowBound;
            this.highBound = highBound;
            this.percentage = percentage;
        }

        public Money taxesOn(Money amount) {
            Money result = Money.ZERO;
            if (amount.compareTo(lowBound) <= 0) {
                result = Money.ZERO;
            } else if (amount.compareTo(highBound) > 0) {
                result = highBound.sub(lowBound).mul(percentage);
            } else {
                result = amount.sub(lowBound).mul(percentage);
            }
            return result;
        }

        public String toString() {
            return String.format("%s, %s, %.2f", lowBound, highBound, percentage);
        }
    }
}
