package Week04;


/**
 * Write a description of class BadNameException here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class BadNameException extends IllegalArgumentException {
    public BadNameException(String message) {
        super(message);
    }
}
