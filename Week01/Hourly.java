package Week01;


/**
 * Write a description of class Hourly here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Hourly extends Employee {
    private Money payRate;
    private double hours;
    
    public Hourly(String name, Money payRate, double hours) {
        super(name);
        this.payRate = payRate;
        this.hours = hours;
    }
    
    public Hourly setPayRate(Money payRate) {
        this.payRate = payRate;
        return this;
    }
    
    public Money getPayRate() {
        return this.payRate;
    }
    
    public void setHoursWorked(double hours) {
        this.hours = hours;
        //return this;
    }
    
    public double getHoursWorked() {
        return this.hours;
    }
    
    @Override
    public Money calculatePay() {
        double worked = hours;
        if (hours > 40) {
            worked = 40 + (1.5 * (hours - 40));
        }
        return payRate.mul(worked);
    }

    @Override
    public Hourly edit(Console console) {
        super.edit(console);
        setPayRate(new Money(console.readDouble(
            "Pay rate", payRate.toDouble())));
        setHoursWorked(console.readDouble("Hours worked", hours));
        return this;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(super.toString());
        builder.append(", ");
        builder.append(String.format("%s:%s, ", "payRate", payRate.toString()));
        builder.append(String.format("%s:%f", "hoursWorked", hours));
        return builder.toString();
    }
}
