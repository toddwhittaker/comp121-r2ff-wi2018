package Week01;


/**
 * Write a description of class Menu here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Menu {
    private String [] items;
    private int itemCount;
    private String title;
    private int maxItemLength;
    
    /**
     * Constructor for objects of class Menu.
     */
    public Menu(String title) {
        items = new String[20];
        itemCount = 0;
        this.title = title.trim();
        maxItemLength = this.title.length();
    }
    
    public void setTitle(String title) {
        this.title = title.trim();
        if (maxItemLength < this.title.length()) {
            maxItemLength = this.title.length();
        }
    }
    
    public void addItem(String item) {
        item = item.trim();
        items[itemCount++] = item;
        if (maxItemLength < item.length()) {
            maxItemLength = item.length();
        }
    }
    
    public String toString() {
        StringBuilder builder = new StringBuilder();
        String itemFormat = "%2d. %s\n";
        builder.append(title + "\n");
        builder.append(repeat(maxItemLength+4, '=') + "\n");
        for (int i = 0; i < itemCount; ++i) {
            builder.append(String.format(itemFormat, i+1, items[i]));
        }
        builder.append(String.format(itemFormat, quitChoice(), "Quit\n"));
        return builder.toString();
    }
    
    private static String repeat(int n, char ch) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < n; ++i) {
            builder.append(ch);
        }
        return builder.toString();
    }
    
    public int quitChoice() {
        return itemCount + 1;
    }
}
