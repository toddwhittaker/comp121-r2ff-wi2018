package Week01;

import java.util.Scanner;
import java.io.PrintStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

/**
 * A line-oriented console for simple input and output. Separate
 * inputs must be separated by line markers (e.g. "\n").
 * 
 * @author Todd Whittaker
 * @version 20160913
 */
public class Console {
    private Scanner input;
    private PrintStream output;
    private ByteArrayOutputStream outputBytes;
    public static final Console std = new Console();
    
    public Console(String testInput) {
        this.outputBytes = new ByteArrayOutputStream();
        this.input = new Scanner(new ByteArrayInputStream(
            testInput.getBytes(StandardCharsets.UTF_8)));
        this.output = new PrintStream(outputBytes);
        
    }
    
    public String getOutput() {
        if (outputBytes != null) {
            return new String(outputBytes.toByteArray(), StandardCharsets.UTF_8);
        }
        return null;
    }
    
    public Console() {
        this(System.in, System.out);
    }
    
    public Console(InputStream in, OutputStream out) {
        this.input = new Scanner(in);
        this.output = new PrintStream(out);
    }
    
    private String nextLineFromInput() {
        String result = input.nextLine();
        // if we're capturing output, echo back the input
        if (outputBytes != null) {
            printf("%s%n", result);
        }
        return result;
    }
    
    public String readLine() {
        return readLine("", null);
    }
    
    public String readLine(String prompt) {
        return readLine(prompt, null);
    }
    
    public String readLine(String prompt, String old) {
        while (true) {
            printf(prompt + (old == null ? "" : " [%s]: "), old);
            String result = nextLineFromInput().trim();
            if ("".equals(result) && old != null) {
                return old;
            } else if (!"".equals(result)) {
                return result;
            } else {
                println("Invalid input.");
            }
        }
    }
    
    public double readDouble() {
        return readDouble("", null);
    }
    
    public double readDouble(String prompt) {
        return readDouble(prompt, null);
    }

    public double readDouble(String prompt, Double old) {
        while (true) {
            printf(prompt + (old == null ? "" : " [%f]: "), old);
            String result = nextLineFromInput().trim();
            if ("".equals(result) && old != null) {
                return old.doubleValue();
            } else {
                try {
                    return Double.parseDouble(result);
                } catch (NumberFormatException e) {
                    printf("Invalid input '%s'.\n", result);
                }
            }
        }
    }

    public int readInt() {
        return readInt("", null);
    }
    
    public int readInt(String prompt) {
        return readInt(prompt, null);
    }
    
    public int readInt(String prompt, Integer old) {
        while (true) {
            printf(prompt + (old == null ? "" : " [%d]: "), old);
            String result = nextLineFromInput().trim();
            if ("".equals(result) && old != null) {
                return old.intValue();
            } else {
                try {
                    return Integer.parseInt(result);
                } catch (NumberFormatException e) {
                    printf("Invalid input '%s'.\n", result);
                }
            }
        }
    }
    
    public Console printf(String format, Object... args) {
        output.printf(format, args);
        return this;
    }

    public void print(Object obj) {
        this.printf("%s", String.valueOf(obj));
    }
    
    public void println(Object obj) {
        this.printf("%s%n", String.valueOf(obj));
    }
    
    public static void main(String [] args) {
        /* demonstration of how it works */
        Console console = Console.std;
        
        console.printf("Read '%s'\n", console.readLine("Enter string -> "));
        console.printf("Read %f\n", console.readDouble("Enter double -> "));
        console.printf("Read %d\n", console.readInt("Enter int -> "));

        console.printf("Read '%s'\n", console.readLine("Enter string", "Hello world"));
        console.printf("Read %f\n", console.readDouble("Enter double", 3.14159));
        console.printf("Read %d\n", console.readInt("Enter int", 42));
    }
}