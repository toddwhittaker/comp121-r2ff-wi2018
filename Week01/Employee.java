package Week01;


/**
 * Write a description of class Employee here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public abstract class Employee {
   
    private String name;
    private int id;
    
    public Employee(String name) {
        this.name = name.trim();
        this.id = -1;
    }
    
    public abstract Money calculatePay();
    
    public void setHireId(int id) {
        this.id = id;
    }
    
    public int getHireId() {
        return this.id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name.trim();
    }
    
    public Employee edit(Console console) {
        setName(console.readLine("Name", this.name));
        return this;
    }
        
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("name:%s, ", this.name));
        if (this.id >= 0) {
            builder.append(String.format("id:%d", this.id));
        }
        return builder.toString();
    }
}
