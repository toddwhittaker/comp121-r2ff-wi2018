package Week03;


/**
 * Write a description of class Utilities here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Utilities {
    public static boolean nullSafeEquals(Object o1, Object o2) {
        // in Excel, =IF(cond, trueResult, falseResult)
        return o1 == null ? o2 == null : o1.equals(o2);
    }
}
