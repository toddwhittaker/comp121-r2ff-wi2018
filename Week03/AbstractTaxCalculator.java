package Week03;
import java.util.List;
import java.util.ArrayList;

/**
 * Write a description of class TaxCalculator here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public abstract class AbstractTaxCalculator implements TaxCalculator {
    List<TaxSchedule> taxSchedules;
    
    public AbstractTaxCalculator() {
        taxSchedules = new ArrayList<TaxSchedule>();
    }
    
    protected void addSchedule(TaxSchedule schedule) {
        taxSchedules.add(schedule);
    }
    
    public Money calculateTax(Money yearlyAmount) {
        Money result = Money.ZERO;
        for (TaxSchedule schedule : taxSchedules) {
            result = schedule.taxesOn(yearlyAmount);
            if (result.toDouble() > 0) {
                return result;
            }
        }
        return result;
    }
    
    

}
