package Week03;

/**
 * Write a description of class Salaried here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Salaried extends AbstractEmployee {
    private Money salary;


    public Salaried(String name, Money salary) {
        super(name);
        this.salary = salary;
    }

    public Money getSalary() {
        return salary;
    }

    public Salaried setSalary(Money salary) {
        this.salary = salary;
        return this;
    }

    @Override
    public Money calculateGrossPay() {
        return salary.div(PAY_PERIODS);
    }

    @Override
    public Salaried getRaise(double percent) {
        this.setSalary(this.getSalary().mul(1+percent));
        return this;
    }

    @Override
    public Salaried edit(Console console) {
        super.edit(console);
        setSalary(new Money(console.readDouble(
                    "Salary", salary.toDouble())));
        return this;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(super.toString());
        builder.append(String.format(
                ", %s:%s", "salary", salary.toString()));
        return builder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Salaried)) {
            return false;
        }
        Salaried other = (Salaried)obj;
        return super.equals(other) &&
            Utilities.nullSafeEquals(this.salary, other.salary);

    }
}
