package Week03;

/**
 * Write a description of class Employee here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public abstract class AbstractEmployee implements Employee {
    protected static final int PAY_PERIODS = 52;
    private String name;
    private int id;

    public AbstractEmployee(String name) {
        this.name = name.trim();
        this.id = -1;
    }
    
    public void setHireId(int id) {
        this.id = id;
    }

    public int getHireId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.trim();
    }

    @Override
    public int compareTo(Employee other) {
        // compare by names
        String myLastName = this.getName().split(" ")[1];
        String otherLastName = other.getName().split(" ")[1];
        return myLastName.compareTo(otherLastName);
    }
    
    public Employee edit(Console console) {
        setName(console.readLine("Name", this.name));
        return this;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("name:%s", this.name));
        if (this.id >= 0) {
            builder.append(String.format(", id:%d", this.id));
        }
        return builder.toString();
    }
    
    @Override
    public boolean equals(Object obj) {
        // Object class does this:
        // return this == obj;
        
        if (obj == null || !(obj instanceof AbstractEmployee)) {
            return false;
        }
        AbstractEmployee emp = (AbstractEmployee)obj;
        return Utilities.nullSafeEquals(this.name, emp.name) &&
            this.id == emp.id;
    }
    
    @Override
    public final Money calculateNetPay(TaxCalculator calc) {
        // calculate gross pay and subtract taxes to get net pay.
        Money gross = calculateGrossPay();
        Money taxes = calc.calculateTax(
            gross.mul(PAY_PERIODS)).div(PAY_PERIODS);
        return gross.sub(taxes);
    }
}
