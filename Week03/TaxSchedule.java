package Week03;

/**
 * Write a description of class TaxSchedule here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class TaxSchedule {
    public Money base;
    public Money upperBound;
    public Money amountOver;
    public double percentage;

    /**
     * Taxes are a base + 
     */
    public TaxSchedule(Money base, Money upperBound, Money amountOver, double percentage) {
        this.base = base;
        this.upperBound = upperBound;
        this.percentage = percentage;
        this.amountOver = amountOver;
    }

    public boolean matches(Money amount) {
        return amount.compareTo(upperBound) <= 0;
    }

    public Money taxesOn(Money amount) {
        if (matches(amount)) {
            return base.add(amount.sub(amountOver).mul(percentage));
        }
        return Money.ZERO;
    }
}
