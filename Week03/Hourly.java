package Week03;

/**
 * Write a description of class Hourly here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Hourly extends AbstractEmployee {
    private Money payRate;
    private double hours;

    public Hourly(String name, Money payRate, double hours) {
        super(name);
        this.payRate = payRate;
        this.hours = hours;
    }

    public Hourly setPayRate(Money payRate) {
        this.payRate = payRate;
        return this;
    }

    public Money getPayRate() {
        return this.payRate;
    }

    @Override
    public Hourly getRaise(double percent) {
        this.setPayRate(this.getPayRate().mul(1+percent));
        return this;
    }

    public void setHoursWorked(double hours) {
        this.hours = hours;
        //return this;
    }

    public double getHoursWorked() {
        return this.hours;
    }

    @Override
    public Money calculateGrossPay() {
        double worked = hours;
        if (hours > 40) {
            worked = 40 + (1.5 * (hours - 40));
        }
        return payRate.mul(worked);
    }

    @Override
    public Hourly edit(Console console) {
        super.edit(console);
        setPayRate(new Money(console.readDouble(
                    "Pay rate", payRate.toDouble())));
        setHoursWorked(console.readDouble("Hours worked", hours));
        return this;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(super.toString());
        builder.append(", ");
        builder.append(String.format("%s:%s, ", "payRate", payRate.toString()));
        builder.append(String.format("%s:%.1f", "hoursWorked", hours));
        return builder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Hourly)) {
            return false;
        }
        Hourly other = (Hourly)obj;
        return super.equals(other) &&
            Utilities.nullSafeEquals(this.payRate, other.payRate) &&
            Math.abs(this.hours - other.hours) < 0.0001;

    }
}
