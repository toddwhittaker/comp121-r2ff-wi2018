package Week03;



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class ScheduleX2018Test.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class ScheduleX2018Test {
    /**
     * Default constructor for test class ScheduleX2018Test.
     */
    public ScheduleX2018Test() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.setUp();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.tearDown();
    }
    
    @Test
    public void testLowBracket() {
        ScheduleX2018 schedule = new ScheduleX2018();
        assertEquals(new Money(100.0), schedule.calculateTax(new Money(1000.0)));
    }
    
    @Test
    public void testSecondBracket() {
        ScheduleX2018 schedule = new ScheduleX2018();
        assertEquals(new Money(2209.5), schedule.calculateTax(new Money(20000.0)));
    }
}
