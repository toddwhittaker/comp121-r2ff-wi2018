package Week03;


/**
 * Write a description of class PrintEmployeeAction here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class PrintEmployeeAction implements MenuAction {
    private Console console;
    private Company company;
    
    public PrintEmployeeAction(Console console, Company company) {
        this.console = console;
        this.company = company;
    }
    public void performAction() {
        console.println(company.toString());
    }
}
