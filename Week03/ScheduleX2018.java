package Week03;


/**
 * Write a description of class ScheduleX2018 here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ScheduleX2018 extends AbstractTaxCalculator {
    /**
     * Constructor for objects of class ScheduleX2018.
     */
    public ScheduleX2018() {
        super();
        super.addSchedule(new TaxSchedule(
            Money.ZERO, new Money(9525.0), Money.ZERO, 0.10));
        super.addSchedule(new TaxSchedule(
            new Money(952.5), new Money(38700.0), new Money(9525.0), 0.12));
    }
}
