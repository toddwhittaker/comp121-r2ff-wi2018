package Week08;

import java.util.Iterator;

/**
 * Write a description of class LinkedCollection here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class LinkedCollection<E> extends AbstractCollection<E> {

    private int size;
    private Link<E> head;
    private Link<E> tail;

    /**
     * Constructor for objects of class LinkedCollection.
     */
    public LinkedCollection() {
        size = 0;
        head = null;
        tail = null;
    }

    public int size() {
        return size;
    }

    public boolean add(E element) {
        Link<E> link = new Link<E>(element);
        if (size == 0) {
            head = tail = link;
        } else {
            tail.next = link;
            tail = link;
        }
        ++size;
        return true;
    }

    public Iterator<E> iterator() {
        return new LinkedCollectionIterator();
    }

    private static final class Link<E> {
        public E data;
        public Link<E> next;

        public Link(E data) {
            this.data = data;
            this.next = null;
        }
    }

    private class LinkedCollectionIterator implements Iterator<E> {

        private Link<E> cursor;
        private Link<E> priorCursor;
        private boolean nextWasCalled;

        public LinkedCollectionIterator() {
            cursor = null;
            priorCursor = null;
            nextWasCalled = false;
        }

        public boolean hasNext() {
            if (cursor == null) {
                return head != null;
            }
            return cursor.next != null;
        }

        public E next() {
            if (!hasNext()) {
                throw new java.util.NoSuchElementException();
            }
            if (cursor == null) {
                cursor = head;
            } else {
                priorCursor = cursor;
                cursor = cursor.next;
            }
            nextWasCalled = true;
            return cursor.data;
        }

        public void remove() {
            if (!nextWasCalled) {
                throw new IllegalStateException();
            }
            // four cases for removal: first thing, last thing, only thing, middle thing
            // I'm removing the head
            if (cursor == head) {
                head = cursor.next;
                // removing the only thing
                if (head == null) {
                    tail = null;
                }
            } else if (cursor == tail) {
                // removing the tail
                tail = priorCursor;
                tail.next = null;
            } else {
                // removing a middle element
                priorCursor.next = cursor.next;
            }
            cursor = priorCursor;
            --size;
            nextWasCalled = false;
        }
    }
}
