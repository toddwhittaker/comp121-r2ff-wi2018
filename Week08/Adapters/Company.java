package Week08.Adapters;

import java.util.List;
import java.util.ArrayList;

/**
 * Write a description of class Company here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Company {
    private List<Employee> employees;
    
    public Company() {
        employees = new ArrayList<Employee>();
        hireHourlyWorkers();
        hireOtherWorkers();
        System.out.println(this);
    }
    
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Employee e : employees) {
            builder.append(String.format("%s\n",e.toString()));
        }
        return builder.toString();
    }
    
    private void hireHourlyWorkers() {
        employees.add(new HourlyEmployee("Fred Weasley", 25.0));
        employees.add(new HourlyEmployee("George Weasley", 30.0));
        employees.add(new HourlyEmployee("Harry Potter", 20.0));
        employees.add(new HourlyEmployee("Severus Snape", 43.00));
    }
    
    private void hireOtherWorkers() {
        Employee albus = new EmployeeAdapter(new OtherEmployee("Albus", "Dumbledore"));
        Employee minerva = new EmployeeAdapter(new OtherEmployee("Minerva", "McGonagal"));
        albus.setPayRate(55.0);
        minerva.setPayRate(45.0);
        employees.add(albus);
        employees.add(minerva);
    }
}
