package Week08.Adapters;

/**
 * Write a description of class OtherEmployee here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class OtherEmployee {

    private String first;
    private String last;
    
    public OtherEmployee(String first, String last) {
        setFirst(first);
        setLast(last);
    }

    public String getFirst() {
        return this.first;
    }

    public String getLast() {
        return this.last;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public double pay(double rate, double hours) {
        double result = hours * rate;
        if (hours > 60) {
            result += (hours - 60) * rate * .5;
        }
        return result;
    }
    
    public String toString() {
        return this.getClass().getSimpleName() + " " + this.first + " " + this.last;
    }
}
