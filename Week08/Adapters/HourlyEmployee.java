package Week08.Adapters;

/**
 * Write a description of class HourlyEmployee here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class HourlyEmployee implements Employee {
    private String name;
    private double rate;

    public HourlyEmployee(String name, double rate) {
        setName(name);
        setPayRate(rate);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Employee setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public Employee setPayRate(double rate) {
        this.rate = rate;
        return this;
    }

    @Override
    public double calculatePay(int hours) {
        double result = hours * rate;
        if (hours > 40) {
            result += (hours - 40) * rate * .5;
        }
        return result;
    }

    public String toString() {
        return getClass().getSimpleName() + " " + this.name;
    }
}
