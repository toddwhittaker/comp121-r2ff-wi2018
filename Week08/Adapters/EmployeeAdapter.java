package Week08.Adapters;

/**
 * Write a description of class EmployeeAdapter here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class EmployeeAdapter implements Employee {
    private OtherEmployee base;
    private double payRate;
    
    public EmployeeAdapter(OtherEmployee base) {
        this.base = base;
    }

    public String getName() {
        return base.getFirst() + " " + base.getLast();
    }

    public Employee setName(String name) {
        String [] names = name.split(" ");
        base.setFirst(names[0]);
        base.setLast(names[1]);
        return this;
    }

    public Employee setPayRate(double rate) {
        this.payRate = rate;
        return this;
    }

    public double calculatePay(int hours) {
        return base.pay(this.payRate, hours);
    }
    
    public String toString() {
        return base.toString();
    }
}
