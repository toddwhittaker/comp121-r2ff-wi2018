package Week08.Adapters;


/**
 * Write a description of interface Employee here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public interface Employee {
    String getName();
    Employee setName(String name);
    Employee setPayRate(double rate);
    double calculatePay(int hours);
    
}
