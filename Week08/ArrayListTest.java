package Week08;

import java.util.List;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class ArrayListTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class ArrayListTest extends AbstractListTest {
    
    private List<Integer> list;
    
    /**
     * Default constructor for test class ArrayListTest.
     */
    public ArrayListTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        super.setUp();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        super.tearDown();
    }
    
    @Override
    protected <T> List<T> getListToTest(Class<T> clazz) {
        return new ArrayList<T>();
    }
    
}
