package Week08;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Write a description of class ArrayCollection here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ArrayCollection<E> extends AbstractCollection<E> {

    private int size;
    private E [] data;

    public ArrayCollection() {
        this(10);
    }

    public ArrayCollection(int initialSize) {
        if (initialSize <= 0) {
            throw new IllegalArgumentException("Must have a postive initial size");
        }
        this.size = 0;
        data = (E[])(new Object[initialSize]);
    }

    /**
     * Returns the number of elements in the collection.
     * @return the number of elements in the collection.
     */
    public int size() {
        return this.size;
    }

    /**
     * Inserts an element into a collection.  Returns true if the collection
     * is changed as a result of the operation, and false otherwise.
     * @param obj the object to add to the collection
     * @return true if the collection is altered
     */
    @Override
    public boolean add(E element) {
        if (size < data.length) {
            data[size] = element;
            ++size;
            return true;
        }
        return false;
    }

    /**
     * Removes all elements from the collection.  The collection will
     * be empty after calling this method.
     */
    @Override
    public void clear() {
        for (int i = 0; i < size; ++i) {
            data[i] = null;
        }
        size = 0;
    }

   /**
     * Returns an iterator over the elements in this collection. There are
     * no guarantees concerning the order in which the elements are returned
     * (unless this collection is an instance of some class that provides a
     * guarantee).
     * 
     * @return an iterator object.
     */
    public Iterator<E> iterator() {
        return new ArrayCollectionIterator();
    }

    private class ArrayCollectionIterator implements Iterator<E> {

        private int index;
        private boolean nextCalled;

        public ArrayCollectionIterator() {
            index = 0;
            nextCalled = false;
        }

        public boolean hasNext() {
            return index < size();
        }

        public E next() {
            if (!hasNext()) {
                throw new NoSuchElementException("Index out of bounds");
            }
            nextCalled = true;
            return data[index++];
        }

        public void remove() {
            if (!nextCalled) {
                throw new IllegalStateException("No prior call to next()");
            }
            // remove the thing at index - 1.
            for (int i = index - 1; i < size() - 1; ++i) {
                data[i] = data[i+1];
            }
            --index;
            data[--size] = null;
            nextCalled = false;
        }
    }
}
