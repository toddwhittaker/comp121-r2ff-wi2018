package Week08;

import java.util.List;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

/**
 * Write a description of class AbstractList here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public abstract class AbstractList<E> extends AbstractCollection<E> implements List<E> {
    // override for efficiency.
    @Override
    public void clear() {
        ListIterator<E> itr = this.listIterator(this.size());
        while (itr.hasPrevious()) {
            itr.previous();
            itr.remove();
        }
    }

    /**
     * Inserts an element into a collection.  Returns true if the collection
     * is changed as a result of the operation, and false otherwise.
     * @param obj the object to add to the collection
     * @return true if the collection is altered
     */
    @Override
    public boolean add(E element) {
        listIterator(size()).add(element);
        return true;
    }

    /**
     * Returns the element of the list located at the specified index.
     * @param index the location of the element
     * @return the element at the index
     */
    public E get(int index) {
        return listIterator(index).next();
    }

    /**
     * Inserts the element into the list at the given location.
     * @param index the location at which to insert
     * @param obj the object to add to the collection
     */
    public void add(int index, E obj) {
        listIterator(index).add(obj);
    }

    /**
     * Returns the location in the list of the first element that matches
     * the parameter object <tt>obj</tt> according to its equals method.
     * @param obj the object for which to search
     * @return the first index (from 0) where the object is found, or -1
     * if the object is not found in the list
     */
    public int indexOf(Object obj) {
        ListIterator<E> itr = listIterator();
        while(itr.hasNext()) {
            if (Utilities.nullSafeEquals(obj, itr.next())) {
                return itr.previousIndex();
            }
        }
        return -1;
    }

    /**
     * Returns the location in the list of the last element that matches
     * the parameter object <tt>obj</tt> according to its equals method.
     * @param obj the object for which to search
     * @return the last index (from 0) where the object is found, or -1
     * if the object is not found in the list
     */
    public int lastIndexOf(Object obj) {
        ListIterator<E> itr = listIterator(size());
        while(itr.hasPrevious()) {
            if (Utilities.nullSafeEquals(obj, itr.previous())) {
                return itr.nextIndex();
            }
        }
        return -1;
    }

    /**
     * Removes the element from the list at the specified index.
     * @param index the index of the object to be removed
     * @returns the thing that was removed
     * @throws IndexOutOfBoundsException if index < 0 or index >= size()
     */
    public E remove(int index) {
        ListIterator<E> itr = listIterator(index);
        E result = itr.next();
        itr.remove();
        return result;
    }

    /**
     * Inserts all the elements of <tt>coll</tt> into this collection at
     * the specified location.  If both this collection and the parameter
     * are the same collection, then the operational behavior is undefined
     * (i.e. bad things can happen).
     * @param index the location at which to insert
     * @param coll the collection from which to draw elements for addition.
     * @return true when this collection is modified as a result.
     */
    public boolean addAll(int index, Collection<? extends E> coll) {
        int size = this.size();
        ListIterator<E> itr = listIterator(index);
        for (E element : coll) {
            itr.add(element);
        }
        return size != this.size();
    }

    /**
     * Returns an iterator over the elements in this collection. There are
     * no guarantees concerning the order in which the elements are returned
     * (unless this collection is an instance of some class that provides a
     * guarantee).
     * 
     * @return an iterator object.
     */
    public Iterator<E> iterator() {
        return listIterator();
    }
}
