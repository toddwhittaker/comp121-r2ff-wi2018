package Week00;

import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class ConsoleTest.
 *
 * @author  Todd Whittaker
 * @version 20160913
 */
public class ConsoleTest {
    /**
     * Default constructor for test class ConsoleTest.
     */
    public ConsoleTest() {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
    }
    
    private InputStream fromString(String input) {
        return new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
    }
    
    private String fromOutStream(ByteArrayOutputStream output) {
        return new String(output.toByteArray(), StandardCharsets.UTF_8);
    }
    
    @Test
    public void testReadLine1() {
        Console console = new Console("First line\nSecond line");
        assertEquals("First line", console.readLine());
        assertEquals("Second line", console.readLine());
        assertTrue(console.getOutput().contains("First line"));
        assertTrue(console.getOutput().contains("Second line"));
    }

    @Test
    public void testReadLine2() {
        Console console = new Console("First line\nSecond line");
        assertEquals("First line", console.readLine("pFirst"));
        assertEquals("Second line", console.readLine("pSecond"));
        assertTrue(console.getOutput().contains("pFirst"));
        assertTrue(console.getOutput().contains("pSecond"));
    }

    @Test
    public void testReadLine3() {
        Console console = new Console("\nSecond line");
        assertEquals("First line", console.readLine("","First line"));
        assertEquals("Second line", console.readLine("pSecond"));
        assertTrue(console.getOutput().contains("pSecond"));
    }
    
    @Test
    public void testReadDouble1() {
        Console console = new Console("3.1415\n2.718");
        assertEquals(3.1415, console.readDouble(), 1e-10);
        assertEquals(2.718, console.readDouble(), 1e-10);
        assertTrue(console.getOutput().contains("3.1415"));
        assertTrue(console.getOutput().contains("2.718"));
    }
    
    @Test
    public void testReadDouble2() {
        Console console = new Console("asdf\n3.1415\n2.718");
        assertEquals(3.1415, console.readDouble(), 1e-10);
        assertEquals(2.718, console.readDouble(), 1e-10);
        assertTrue(console.getOutput().contains("Invalid"));
    }

    @Test
    public void testReadInt() {
        Console console = new Console("asdf\n42\n111");
        assertEquals(42, console.readInt());
        assertEquals(111, console.readInt());
        assertTrue(console.getOutput().contains("Invalid"));
    }

}