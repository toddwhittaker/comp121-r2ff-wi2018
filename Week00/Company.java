package Week00;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;

/**
 * Write a description of class Company here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Company {
    private Employee [] employees;
    private int size;
    private int nextId;

    public Company() {
        employees = new Employee[100];
        size = 0;
        nextId = 0;
    }

    public int getHeadCount() {
        return size;
    }

    public boolean hire(Employee e) {
        if (size < employees.length && e.getHireId() < 0) {
            employees[size++] = e;
            e.setHireId(nextId++);
            return true;
        }
        return false;
    }

    private int findEmployeeById(int id) {
        for (int i = 0; i < size; ++i) {
            if (employees[i].getHireId() == id) {
                return i;
            }
        }
        return -1;
    }

    public boolean fire(int id) {
        if (id < 0) {
            return false;
        }
        int index = findEmployeeById(id);
        if (index >= 0) {
            employees[index].setHireId(-1);
            employees[index] = employees[size-1];
            employees[size-1] = null;
            --size;
            return true;
        }
        return false;
    }

    public void editEmployee(int id, Console console) {
        int index = findEmployeeById(id);
        if (index >= 0) {
            employees[index].edit(console);
        }
    }

    public String payAll() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size; ++i) {
            builder.append(String.format("Pay %s %s\n",
                employees[i].getName(), employees[i].calculatePay()));
        }
        return builder.toString();
    }
    
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size; ++i) {
            builder.append(String.format("%2d. %s\n", i+1, employees[i].toString()));
        }
        return builder.toString();
    }

    public static Employee createRandomEmployee() {
        if (names.size() > 0) {
            Random r = new Random();
            Money rate = Money.ZERO;
            double hours = 0;

            int type = r.nextInt(2) + 1;
            String name = names.get(r.nextInt(names.size()));

            names.remove(name);
            if (type == Employee.HOURLY) {
                rate = new Money(r.nextDouble()*30 + 20);
                hours = r.nextInt(30) + 20;
            } else if (type == Employee.SALARIED) {
                rate = new Money(r.nextDouble()*40000 + 40000);
            }
            return new Employee(name, type, rate, hours);
        }        
        return null;
    }

    private static final List<String> names = new ArrayList<String>(
        java.util.Arrays.asList(new String [] {
            "Albus Dumbledore",
            "Minerva McGonnagal",
            "Severus Snape",
            "Filius Flitwick",
            "Horace Slughorn",
            "Argus Filch",
            "Gilderoy Lockhart",
            "Quirinus Quirrell",
            "Pomana Sprout",
            "Sybill Trelawney",
            "Rubeus Hagrid",
            "Charity Burbage",
            "Rolanda Hooch",
            "Remus Lupin",
            "Alastor Moody",
            "Poppy Pomfrey",
            "Dolores Umbridge"
        }));

}
