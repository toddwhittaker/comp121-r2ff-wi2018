package Week00;

/**
 * Write a description of class Driver here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Driver {
    private Menu menu;
    private Console console;
    private Company company;

    private Driver() {
        menu = new Menu("Company menu");
        menu.addItem("List employees");
        menu.addItem("Hire employee");
        menu.addItem("Fire employee");
        menu.addItem("Edit employee");
        menu.addItem("Pay employees");
        console = new Console();
        company = new Company();
        for (int i = 0; i < 10; ++i) {
            company.hire(Company.createRandomEmployee());
        }
    }    

    private void processChoice(int choice) {
        if (choice <= 0 || choice > menu.quitChoice()) {
            console.println("Invalid selection.\n");
            return;
        }

        switch (choice) {
            case 1 :
                console.println(company.toString());
                break;
            case 2 :
                company.hire(makeEmployee());
                break;
            case 3 :
                company.fire(console.readInt("Enter employee id", -1));
                break;
            case 4 :
                company.editEmployee(console.readInt("Enter employee id", -1), console);
                break;
            case 5 :
                console.println(company.payAll());
                break;
        }
    }

    private Employee makeEmployee() {
        int type = -1;
        while (type != Employee.HOURLY && type != Employee.SALARIED) {
            type = console.readInt("Type (1=hourly, 2=salaried): ");
        }
        Employee e = new Employee("", type, Money.ZERO, 0);
        e.edit(console);
        return e;
    }
    
    private void menuLoop() {
        int choice = 0;
        do {
            console.print(menu.toString());
            choice = console.readInt(String.format("Enter choice (1-%d): ",
                menu.quitChoice()));
            processChoice(choice);
        } while (choice != menu.quitChoice());
        console.print("Bye!\n");
    }

    public static void main(String [] args) {
        Driver driver = new Driver();
        driver.menuLoop();
    }
}
