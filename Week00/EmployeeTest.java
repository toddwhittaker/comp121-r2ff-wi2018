package Week00;



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class EmployeeTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class EmployeeTest {
    /**
     * Default constructor for test class EmployeeTest.
     */
    public EmployeeTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.setUp();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.tearDown();
    }
    
    @Test
    public void testConstruction() {
        Employee e = new Employee("Albus Dumbledore",
            Employee.SALARIED, new Money(52000.0), 0);
        assertNotNull(e);
    }
    
    @Test
    public void testSalariedCalculatePay() {
        Employee e = new Employee("Albus Dumbledore",
            Employee.SALARIED, new Money(52000.0), 0);
        assertEquals(new Money(1000.0), e.calculatePay());
    }
    
    @Test
    public void testHourlyCalculatePay1() {
        Employee e = new Employee("Albus Dumbledore",
            Employee.HOURLY, new Money(10.0), 30);
        assertEquals(new Money(300.0), e.calculatePay());
    }
    
    @Test
    public void testHourlyCalculatePay2() {
        Employee e = new Employee("Albus Dumbledore",
            Employee.HOURLY, new Money(10.0), 40);
        assertEquals(new Money(400.0), e.calculatePay());
    }

    @Test
    public void testHourlyCalculatePay3() {
        Employee e = new Employee("Albus Dumbledore",
            Employee.HOURLY, new Money(10.0), 50);
        assertEquals(new Money(550.0), e.calculatePay());
    }
    
    @Test
    public void testToString1() {
        Employee e = new Employee("Albus Dumbledore",
            Employee.HOURLY, new Money(10.0), 50);
        String s = e.toString();
        assertTrue(s.contains("name:Albus Dumbledore"));
        assertTrue(s.contains("type:HOURLY"));
        assertTrue(s.contains("payRate:$10.00"));
        assertTrue(s.contains("hours:50"));
    }

    @Test
    public void testToString2() {
        Employee e = new Employee("Albus Dumbledore",
            Employee.SALARIED, new Money(52000.0), 50);
        String s = e.toString();
        assertTrue(s.contains("name:Albus Dumbledore"));
        assertTrue(s.contains("type:SALARIED"));
        assertTrue(s.contains("salary:$52000.00"));
        assertFalse(s.contains("hours"));
    }
}
