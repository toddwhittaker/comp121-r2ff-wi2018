package Week00;


/**
 * Write a description of class Employee here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Employee {
    public static final int HOURLY = 1;
    public static final int SALARIED = 2;
    
    private String name;
    private int type;
    private Money payRate;
    private double hours;
    private int id;
    
    public Employee(String name, int type, Money payRate, double hours) {
        this.name = name.trim();
        this.payRate = payRate;
        this.hours = hours;
        this.type = type;
        this.id = -1;
    }
    
    public Money calculatePay() {
        final int WEEKS_PER_YEAR = 52;
        Money result = Money.ZERO;
        if (type == HOURLY) {
            double worked = hours;
            if (hours > 40) {
                worked = 40 + (1.5 * (hours - 40));
            }
            result = payRate.mul(worked);
        } else if (type == SALARIED) {
            result = payRate.div(WEEKS_PER_YEAR);
        }
        return result;
    }
    
    public void setHireId(int id) {
        this.id = id;
    }
    
    public int getHireId() {
        return this.id;
    }
    
    public String getName() {
        return name;
    }
    
    public Employee edit(Console console) {
        name = console.readLine("Name", this.name);
        if (type == Employee.HOURLY) {
            payRate = new Money(console.readDouble("payRate", payRate.toDouble()));
            hours = console.readDouble("hours", hours);
        } else if (type == Employee.SALARIED) {
            payRate = new Money(console.readDouble("salary", payRate.toDouble()));
        }
        return this;
    }
        
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("name:%s, ", this.name));
        if (this.id >= 0) {
            builder.append(String.format("id:%d, ", this.id));
        }
        if (this.type == HOURLY) {
            builder.append(String.format("type:HOURLY, payRate:%s, hours:%3.1f",
                payRate.toString(), hours));
        } else if (this.type == SALARIED) {
            builder.append(String.format("type:SALARIED, salary:%s",
                payRate.toString()));
        }
        return builder.toString();
    }
}
