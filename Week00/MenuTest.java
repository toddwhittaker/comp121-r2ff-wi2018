package Week00;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class MenuTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class MenuTest {
    private Menu menu;
    
    /**
     * Default constructor for test class MenuTest.
     */
    public MenuTest() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super();
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.setUp();
        menu = new Menu("This is the title");
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() {
        /*# TODO: uncomment if you are extending a base unit test */
        // super.tearDown();
    }

    @Test
    public void testConstruction() {
        assertNotNull(menu);
        assertTrue(menu.toString().contains("This is the title"));
        assertTrue(menu.toString().contains(" 1. Quit"));
        assertEquals(1, menu.quitChoice());
    }
    
    @Test
    public void testChangeTitle() {
        menu.setTitle("A brand new title");
        assertTrue(menu.toString().contains("A brand new title"));
    }
    
    @Test
    public void testAddItems() {
        menu.addItem("The first thing to do");
        menu.addItem("The second thing to do");
        assertTrue(menu.toString().contains(" 1. The first thing to do"));
        assertTrue(menu.toString().contains(" 2. The second thing to do"));
        assertEquals(3, menu.quitChoice());
    }
}
